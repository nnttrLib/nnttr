/* global __dirname, require, module*/

const webpack = require('webpack');
const UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
const path = require('path')
const env  = require('yargs').argv.env // use --env with webpack 2

var fs = require('fs')
var nodeModules = {}
fs.readdirSync('node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1
  })
  .forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod
  })

let libraryName = 'server'

let plugins = [], outputFile

plugins.push(  new webpack.ProvidePlugin({
  "React": "react",
}))

if (env === 'build') {
  plugins.push(new UglifyJsPlugin({ minimize: true }))
  outputFile = libraryName + '.js'
} else {
  outputFile = libraryName + '.js'
}

const config = {
  entry: {
    app: [
      path.join(__dirname, 'src/server.js')
    ],
  },

    devtool: 'source-map',
    output: {
        path: __dirname + '/lib',
        filename: outputFile,
        library: libraryName,
        libraryTarget: 'umd',
        umdNamedDefine: true
    },
    module: {
        rules: [
            {
                test: /(\.jsx|\.js)$/,
                loader: 'babel-loader',
                exclude: /(node_modules|bower_components)/
            },
        ]
    },
    plugins: plugins,
    externals: nodeModules,

};

module.exports = config;