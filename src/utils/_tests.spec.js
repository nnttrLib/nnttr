/* global describe, it */

import chai from 'chai'

import nnttr from '../../lib/nnttr.js'

chai.expect()

const expect = chai.expect

let utils = nnttr.utils

describe('utils', () => {
  it('Ok', () => {
    expect(utils).to.be.a('object')
  })
  describe('Callapi', () => {
    // Pendiente de definir Tests
  })
  describe('Crypto', () => {
    // Pendiente de definir Tests
  })
  describe('Ducks', () => {
    // Pendiente de definir Tests
  })
  describe('helpers', () => {
    let nombres = [
      {
        nombre: 'Joaquin',
        apellido: 'Sanchez'
      },
      {
        nombre: 'Pepe',
        apellido: 'Diaz'
      },
      {
        nombre: 'Angela',
        apellido: 'Garcia'
      },
      {
        nombre: 'Lucas',
        apellido: 'Diaz'
      },
      {
        nombre: 'Beatriz',
        apellido: 'Fernandez'
      }
    ]
    let result
    let resultTest

    it('capitalizeFirstLetter', () => {
      expect(utils.helpers.capitalizeFirstLetter('hola mundo')).to.be.equal('Hola mundo')
      expect(utils.helpers.capitalizeFirstLetter()).to.be.equal('')
    })

    it('dynamicSort', () => {
      // Ordenamos el array de objetos por la propiedad que nos interesa en ascendente
      result = nombres.sort(utils.helpers.dynamicSort('nombre'))
      // Lo pasamos a un array para hacer la comprobación
      result = result.map(item => item.nombre)
      resultTest = ['Angela', 'Beatriz', 'Joaquin', 'Lucas', 'Pepe']
      expect(result).deep.equal(resultTest)
      // Ahora en descendente
      result = nombres.sort(utils.helpers.dynamicSort('-nombre'))
      result = result.map(item => item.nombre)
      resultTest = ['Pepe', 'Lucas', 'Joaquin', 'Beatriz', 'Angela']
      expect(result).deep.equal(resultTest)
    })

    it('dynamicSortMultiple', () => {
      result = nombres.sort(utils.helpers.dynamicSortMultiple('apellido', 'nombre'))
      result = result.map(item => item.apellido + ' ' + item.nombre)
      resultTest = ['Diaz Lucas', 'Diaz Pepe', 'Fernandez Beatriz', 'Garcia Angela', 'Sanchez Joaquin']
      expect(result).deep.equal(resultTest)
      // Ahora con un campo en descendente
      result = nombres.sort(utils.helpers.dynamicSortMultiple('-apellido', 'nombre'))
      result = result.map(item => item.apellido + ' ' + item.nombre)
      resultTest = ['Sanchez Joaquin', 'Garcia Angela', 'Fernandez Beatriz', 'Diaz Lucas', 'Diaz Pepe']
      expect(result).deep.equal(resultTest)
    })
  })

  /* Pendiente de integrar un servidor Api para pruebas
  describe('CallApi', () => {
    let callApi = new utils.CallApi('http://srvv-local:50001')

    it('Ok ', () => {
      expect(callApi).to.be.a('object')
    })
    let registro1 = {
      id: 100,
      nombre: 'nombre Post',
      descripcion: 'Prueba de inserción '
    }
    it('post', (done) => {
      callApi.post('/api/public._test', registro1, false)
        .then((res) => {
          expect(res.succesful).to.be.equal(true)
          done()
        })
    })
    it('get', (done) => {
      callApi.get('/api/public._test', false)
        .then((res) => {
          expect(res.succesful).to.be.equal(true)
          expect(res.registros.length).to.be.equal(1)
          expect(res.registros[0].nombre).to.be.equal('nombre Post')
          done()
        })
    })
    it('put', (done) => {
      let registro2 = {
        nombre: 'nombre Put',
        descripcion: 'Prueba de inserción '
      }
      callApi.put('/api/public._test', 100, registro2, false)
        .then((res) => {
          expect(res.succesful).to.be.equal(true)
          done()
        })
        .catch((err) => done(err))
    })
    it('delete', (done) => {
      callApi.delete('/api/public._test', 100, false)
        .then((res) => {
          expect(res.succesful).to.be.equal(true)
          done()
        })
    })
  })
  */
})
