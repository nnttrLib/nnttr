import crypto from 'crypto'
import LZString from 'lz-string'

class Crypto {
  constructor (secretPassword = 'MiPalabraSuperSecreta', algorithm = 'aes-256-ctr') {
    this._secretPassword = secretPassword
    this._algorithm = algorithm
  }

  encrypt (text) {
    let cipher = crypto.createCipher(this._algorithm, this._secretPassword)
    let crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex')
    return crypted
  }

  decrypt (text) {
    let decipher = crypto.createDecipher(this._algorithm, this._secretPassword)
    let dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8')
    return dec
  }

  encryptCompress (text) {
    return LZString.compressToUTF16((this.encrypt(text)))
  }

  decryptCompress (text) {
    return this.decrypt(LZString.decompressFromUTF16(text))
  }
}

export default Crypto
