import CallApi from './CallApi'
import Ducks from './Ducks'
import Crypto from './Crypto'
import * as helpers from './helpers'

export default {
  CallApi,
  ducks: new Ducks(),
  Crypto,
  helpers
}
