// **********************************************************************************************
// **  Utilizades para cadenas
// **********************************************************************************************
export function capitalizeFirstLetter (string) {
  let cadena = string || ''
  return cadena && cadena[0].toUpperCase() + cadena.slice(1).toLowerCase()
}

export function capitalizeEachWord (str) {
  return str.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
  })
}

// **********************************************************************************************
// **  Utilizades matemáticas
// **********************************************************************************************

// Retorna un entero aleatorio entre min (incluido) y max (excluido)
// ¡Usando Math.round() te dará una distribución no-uniforme!
export function getRandomInt (min, max) {
  return Math.floor(Math.random() * (max - min)) + min
}

// **********************************************************************************************
// **  Utilizades para Arrays
// **********************************************************************************************

// Ejemplo de llamada
// People.sort(dynamicSort("Surname")); Ascendente
// People.sort(dynamicSort("-Surname")); Descendente
export function dynamicSort (property) {
  let sortOrder = 1
  if (property[0] === '-') {
    sortOrder = -1
    property = property.substr(1)
  }
  return function (a, b) {
    let result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0
    return result * sortOrder
  }
}

export function dynamicSortMultiple () {
  /*
   * save the arguments object as it will be overwritten
   * note that arguments object is an array-like object
   * consisting of the names of the properties to sort by
   */
  let props = arguments
  return function (obj1, obj2) {
    let i = 0
    let result = 0
    let numberOfProperties = props.length
    /* try getting a different result from 0 (equal)
     * as long as we have extra properties to compare
     */
    while (result === 0 && i < numberOfProperties) {
      result = dynamicSort(props[i])(obj1, obj2)
      i++
    }
    return result
  }
}

// **********************************************************************************************
// **  Otras Utilidades
// **********************************************************************************************
export function storageAvailable (type) {
  try {
    let storage = window[type]
    let x = '__storage_test__'
    storage.setItem(x, x)
    storage.removeItem(x)
    return true
  } catch (e) {
    return false
  }
}
