/* global fetch:false */
import 'isomorphic-fetch'

function storageAvailable (type) {
  try {
    let storage = window[type]
    let x = '__storage_test__'
    storage.setItem(x, x)
    storage.removeItem(x)
    return true
  } catch (e) {
    return false
  }
}

class CallApi {
  constructor (urlBase, authenticated = true) {
    this.callApi = this.callApi.bind(this)
    this.login = this.login.bind(this)
    this.post = this.post.bind(this)
    this.delete = this.delete.bind(this)
    this.put = this.put.bind(this)

    this._urlBase = urlBase
    this._authenticated = authenticated
    this._configFetchInitial = {}
    this._token = null
  }

  // Propiedades
  get urlBase () {
    return this._urlBase
  }

  set urlBase (urlBase) {
    this._urlBase = urlBase
  }

  set token (token) {
    this._token = token
  }

  // Métodos
  callApi (endPoint, configFetch, authenticated = this._authenticated) {
    let consultaApi = this._urlBase + endPoint

    // duplicamos la configuración del fetch para modificarla añadiéndole el token si debe ser authenticado
    let options
    options = Object.assign({}, configFetch)
    if (authenticated) {
      let token = storageAvailable('localStorage') ? window.localStorage.getItem('token') || null : this._token
      if (token) {
        // Comprobamos si hemos pasado el propiedad headers en configFetch
        if (options.headers) {
          // Si se ha pasado simplemente añadimos / modificamos la propiedad Authorization
          options.headers.Authorization = `Bearer ${token}`
        } else {
          // Si no se ha pasado headers, creamos la propiedad con la clave Authorization
          options.headers = {'Authorization': `Bearer ${token}`}
        }
      } else {
        throw Error('No existe el token')
      }
    }

    return fetch(consultaApi, options)
  }

  login (userName, password, idApp) {
    let configApiLogin = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      },
      body: `username=${userName}&password=${password}&id_app=${idApp}`
    }
    return this.callApi('/login', configApiLogin, false)
  }

  get (endPoint, authenticated) {
    let options = {
      method: 'GET'
    }
    return this.callApi(endPoint, options, authenticated)
      .then(response => response.json())
      .then(json => {
        if (!json.succesful) {
          return Promise.reject(json)
        } else {
          return Promise.resolve(json)
        }
      })
      .catch(error => {
        return Promise.reject(error)
      })
  }

  post (endPoint, data, authenticated) {
    let body = {
      payload: data
    }
    let options = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(body)
    }

    return this.callApi(endPoint, options, authenticated)
      .then(response => response.json())
      .then(json => {
        if (!json.succesful) {
          return Promise.reject(json)
        } else {
          for (let i = 0; i < json.campos.length; i++) {
            data[json.campos[i]] = json.registros[0][json.campos[i]]
          }
          return Promise.resolve(json)
        }
      })
      .catch(error => {
        return Promise.reject(error)
      })
  }

  put (endPoint, id, data, authenticated) {
    let body = {
      payload: data,
      meta: undefined
    }

    let options = {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(body)
    }

    return this.callApi(endPoint + '/' + id, options, authenticated)
      .then(response => response.json())
      .then(json => {
        if (!json.succesful) {
          return Promise.reject(json)
        } else {
          return Promise.resolve(json)
        }
      })
      .catch(error => {
        return Promise.reject(error)
      })
  }

  delete (endPoint, id, authenticated) {
    let options = {
      method: 'DELETE'
    }

    return this.callApi(endPoint + '/' + id, options, authenticated)
      .then(response => response.json())
      .then(json => {
        if (!json.succesful) {
          return Promise.reject(json)
        } else {
          return Promise.resolve(json)
        }
      })
      .catch(error => {
        return Promise.reject(error)
      })
  }
}

export default CallApi
