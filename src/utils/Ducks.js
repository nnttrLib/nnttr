class Ducks {
  constructor () {
    this._typesState = {
      session: 'session',
      data: 'data',
      control: 'control'
    }
  }

  get typesState () {
    return this._typesState
  }

  createAction (type) {
    return function actionCreator (payload, meta) {
      const action = {
        type
      }

      if (payload instanceof Error) {
        action.error = true
      }

      if (payload) action.payload = payload

      if (meta) action.meta = meta

      return action
    }
  }

  createReducer (cases, defaultState = {}) {
    return function reducer (state = defaultState, action = {}) {
      if (state === undefined) return defaultState
      for (const caseName in cases) {
        if (action.type === caseName) return cases[caseName](state, action)
      }
      return state
    }
  }
}

export default Ducks
