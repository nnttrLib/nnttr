const createUpdateWithFragment = (params, dataWriteQueryFunction) => {
  const { mutationName, insertLast, fragmentRoot } = params
  return (proxy, resultMutation) => {
    // En resultMutation nos llega el resultado de la mutación, o el resultado de optimisticResponse
    const newData = resultMutation.data[mutationName]
    const idFragment = fragmentRoot.typeName + '_' + newData[fragmentRoot.nameKeyField]

    let data = proxy.readFragment({
      id: idFragment,
      fragment: fragmentRoot.fragment,
      fragmentName: fragmentRoot.fragmentName
    })

    if (data === null) {
      console.error('Error en UpdatWithFragment')
      console.error(fragmentRoot.fragment)
      console.error(fragmentRoot.fragmentName)
      throw new Error('Error en updateWithFragment al buscar ' + newData[fragmentRoot.nameKeyField])
    }

    if (dataWriteQueryFunction && typeof dataWriteQueryFunction === 'function') {
      // Si hemos pasado un método dataWriteQueryFunction lo utilizamos para actualizar los datos
      data = dataWriteQueryFunction(data, newData, params, proxy)
      if (!data) {
        throw new Error('Error al actualizar datos: ' + JSON.stringify(newData, undefined, 2))
      }
    } else {
      if (insertLast) { // Lo añadimos al final del array
        if (fragmentRoot.fieldNameDataCache) {
          // Si hemos pasado un valor en fieldNameDataCache entonces la actualización
          // es un campo del fragment.
          data[fragmentRoot.fieldNameDataCache].push(newData)
        } else {
          // Si no lo hemos pasado, la actualización es directamente en el fragment.
          data[fragmentRoot].push(newData)
        }
      } else { // Lo añadimos al principio del array
        if (fragmentRoot.fieldNameDataCache) {
          data[fragmentRoot.fieldNameDataCache].unshift(newData)
        } else {
          data[fragmentRoot].unshift(newData)
        }
      }
    }

    proxy.writeFragment({
      id: idFragment,
      fragment: fragmentRoot.fragment,
      fragmentName: fragmentRoot.fragmentName,
      data: data
    })
  }
}

const createUpdateWithQuery = (params, input, query, variablesQuery, dataWriteQueryFunction) => {
  const { mutationName, fieldNameDataCache, insertLast } = params

  return (proxy, resultMutation) => {
      // Este método se ejecuta después de ejecutar la mutación
      // Si hemos definido un optimisticResponse el update se ejecutará 2 veces
      // una nada más llamar a la mutación con los datos que le indiquemos en optimisticResponse
      // y otra una vez que el servidor haya realizado los camibos y haya devuelto la respuesta.

      // En resultMutation nos llega el resultado de la mutación, o el resultado de optimisticResponse
      const newData = resultMutation.data[mutationName]

      // Ahora bucamos en la caché del cliente el registro que queremos modificar.
      // Para ello el método readQuery necesita la query y las variables utilizadas para ejecutarla.
      // Nos devuelve la estructura de datos de la query par apoder moficicarla y luego volver a
      // actualizar la caché con el método writeQuery
      let data = proxy.readQuery({
        query: query,
        variables: variablesQuery
      })

      if (dataWriteQueryFunction && typeof dataWriteQueryFunction === 'function') {
        // Si hemos pasado un método dataWriteQueryFunction lo utilizamos para actualizar los datos
        data = dataWriteQueryFunction(data, newData, params, proxy)
        if (!data) {
          throw new Error('Error al actualizar datos: ' + JSON.stringify(newData, undefined, 2))
        }
      } else {
        // Si no hemos pasado una función para gestionar la actualización de los datos
        // utilizamos la forma estándar.
        if (insertLast) { // Lo añadimos al final del array
          data[fieldNameDataCache].push(newData)
        } else { // Lo añadimos al principio del array
          data[fieldNameDataCache].unshift(newData)
        }
      }

      // Y escribimos los nuevos datos en la caché del cliente.
      // Al hacer esto se renderizaran todos aquellos compoenntes react que dependan de esta query
      proxy.writeQuery({
        query: query,
        variables: variablesQuery,
        data })
  }
}

export const create = (params, input, query, variablesQuery, calcFieldsOptimisticResponse = {}, dataWriteQueryFunction, refetchQueries) => {
  const { mutationName, nameKeyField, typeName } = params

  // Por defecto utilizo OptimisticResponse
  let allowOptimisticResponse = params.hasOwnProperty('allowOptimisticResponse') ? params.allowOptimisticResponse : true

  // Parte común del objeto options, donde definimos las propiedades: variables, optimisticResponse y refetchQueries
  let options = {
    variables: {
      // La propiedad o pripiedades tienen que tener el mismo nombre que las variables
      // que se hayan definido en la mutatión, en este caso sólo se ha definido una variable
      input: {
        ...input  // Pasamos todos los campos del objeto input
      }
    },
    refetchQueries: refetchQueries
  }

  if (allowOptimisticResponse === true ) {
    options.optimisticResponse = {
      // Tiene que haber una propiedad con el mismo nombre que la mutation y con los campos que
      // queremos que tenga el registro de manera inmediata en la parte del cliente
      // Los valores definitivos se actualizarán cuando el servidor termine de procesar la insert
      __typename: 'Mutation',
      [mutationName]: {
        ...input,
        // Además de los campos del input añadimos la clave primaria con un número aleatorio y negativo
        // para controlar que es un dato "provisional", hasta que llegue el dato bueno desde el servidor.
        [nameKeyField]: Math.round(Math.random() * -1000000),

        // Si hay campos calculados y los pasamos se añaden para evitar warnings
        ...calcFieldsOptimisticResponse,

        // Añadimos un campo para indiar el typename de este registro
        // este campo lo añade automáticamente el servidor grahql en su respuesta
        // pero nosotros estamos "construyendo" una respuesta inmediata y para que los datos sean
        // consistentes tenemos que informar del tipo de datos que estamos añadiendo a la caché del cliente
        __typename: typeName
      }
    }
  }

  // Le añadimos el método update, dependiendo de si hemos pasado un campo fragmentRoot en params,
  // añadimos un update u otro.
  if (params.fragmentRoot) {
    options.update = createUpdateWithFragment(params, dataWriteQueryFunction)
  } else {
    options.update = createUpdateWithQuery(params, input, query, variablesQuery, dataWriteQueryFunction)
  }

  return options
}

const removeUpdateWithFragment =  (params, id, dataWriteQueryFunction) => {
  const { nameKeyField, mutationName, fragmentRoot } = params
  return (proxy, resultMutation) => {
    const dataDeleted = resultMutation.data[mutationName]
    const idFragment = fragmentRoot.typeName + '_' + dataDeleted[fragmentRoot.nameKeyField]
    let data = proxy.readFragment({
      id: idFragment,
      fragment: fragmentRoot.fragment,
      fragmentName: fragmentRoot.fragmentName
    })

    if (data === null) {
      console.error('Error en removeUpdateWithFragment')
      console.error(fragmentRoot.fragment)
      console.error(fragmentRoot.fragmentName)
      throw new Error('Error en removeUpdateWithFragment al buscar ' + dataDeleted[fragmentRoot.nameKeyField])
    }

    if (dataWriteQueryFunction && typeof dataWriteQueryFunction === 'function') {
      data = dataWriteQueryFunction(data, dataDeleted, params, proxy)
      if (!data) {
        throw new Error('Error al eliminar datos: ' + JSON.stringify(dataDeleted, undefined, 2))
      }
    } else {
      const indexDataDeleted = data[fragmentRoot.fieldNameDataCache].findIndex(item => item[nameKeyField] === id)
      if (indexDataDeleted > -1) {
        data[fragmentRoot.fieldNameDataCache].splice(indexDataDeleted, 1)
      } else {
        throw new Error('No se ha encontrado el registro con el id: ' + id)
      }
    }

    proxy.writeFragment({
      id: idFragment,
      fragment: fragmentRoot.fragment,
      fragmentName: fragmentRoot.fragmentName,
      data: data
    })
  }
}

const removeUpdateWithQuery =  (params, id, query, variablesQuery, dataWriteQueryFunction) => {
  const { mutationName, nameKeyField, fieldNameDataCache } = params

  return (proxy, resultMutation) => {
    const dataDeleted = resultMutation.data[mutationName]

    let data = proxy.readQuery({
      query: query,
      variables: variablesQuery
    })

    if (dataWriteQueryFunction && typeof dataWriteQueryFunction === 'function') {
      data = dataWriteQueryFunction(data, dataDeleted, params, proxy)
      if (!data) {
        throw new Error('Error al eliminar datos: ' + JSON.stringify(dataDeleted, undefined, 2))
      }
    } else {
      const indexDataDeleted = data[fieldNameDataCache].findIndex(item => item[nameKeyField] === id)
      if (indexDataDeleted > -1) {
        data[fieldNameDataCache].splice(indexDataDeleted, 1)
      } else {
        throw new Error('No se ha encontrado el registro con el id: ' + id)
      }
    }

    proxy.writeQuery({
      query: query,
      variables: variablesQuery,
      data
    })
  }
}

export const remove = (params, id, query, variablesQuery, calcFieldsOptimisticResponse = {}, dataWriteQueryFunction, refetchQueries) => {
  const { nameKeyField, mutationName, variableInputName, typeName } = params
  // Por defecto utilizo OptimisticResponse
  let allowOptimisticResponse = params.hasOwnProperty('allowOptimisticResponse') ? params.allowOptimisticResponse : true

  const variableName = variableInputName || nameKeyField

  let options = {
    variables: {
      [variableName]: id
    },
    refetchQueries: refetchQueries
  }

  if (allowOptimisticResponse === true ) {
    options.optimisticResponse = {
      __typename: 'Mutation',
      [mutationName]: {
        ...calcFieldsOptimisticResponse,
        __typename: typeName
      }
    }
  }

    // Le añadimos el método update, dependiendo de si hemos pasado un campo fragmentRoot en params,
  // añadimos un update u otro.
  if (params.fragmentRoot) {
    options.update = removeUpdateWithFragment(params, id, dataWriteQueryFunction)
  } else {
    options.update = removeUpdateWithQuery(params, id, query, variablesQuery, dataWriteQueryFunction)
  }

  return options
}

const updateUpdateWithFragment = (params, set, id, dataWriteQueryFunction) => {
  const { mutationName, fragmentRoot } = params

  return (proxy, resultMutation) => {
    const dataEdited = resultMutation.data[mutationName]
    const idFragment = fragmentRoot.typeName + '_' + id

    let data = proxy.readFragment({
      id: idFragment,
      fragment: fragmentRoot.fragment,
      fragmentName: fragmentRoot.fragmentName
    })

    if (data === null) {
      console.error('Error en updateUpdateWithFragment')
      console.error(fragmentRoot.fragment)
      console.error(fragmentRoot.fragmentName)
      throw new Error('Error en updateUpdateWithFragment al buscar ' + id)
    }

    if (dataWriteQueryFunction && typeof dataWriteQueryFunction === 'function') {
      data = dataWriteQueryFunction(data, dataEdited, params, proxy)
      if (!data) {
        throw new Error('Error al Actualizar datos: ' + JSON.stringify(dataEdited, undefined, 2))
      }
    } else {
      Object.assign(data, dataEdited)
    }

    proxy.writeFragment({
      id: idFragment,
      fragment: fragmentRoot.fragment,
      fragmentName: fragmentRoot.fragmentName,
      data: data
    })
  }
}

const updateUpdateWithQuery = (params, set, id, query, variablesQuery, dataWriteQueryFunction) => {
  const { mutationName, nameKeyField, fieldNameDataCache } = params

  return (proxy, resultMutation) => {
    const dataEdited = resultMutation.data[mutationName]

    let data = proxy.readQuery({
      query: query,
      variables: variablesQuery
    })

    if (dataWriteQueryFunction && typeof dataWriteQueryFunction === 'function') {
      data = dataWriteQueryFunction(data, dataEdited, params)
      if (!data) {
        throw new Error('Error al Actualizar datos: ' + JSON.stringify(dataEdited, undefined, 2))
      }
    } else {
      if (Array.isArray(data[fieldNameDataCache])) {
        // Si es un array, busacmos el elemento para asignarle lo que hay en dataEdited
        const indexDataEdited = data[fieldNameDataCache].findIndex(item => item[nameKeyField] === id)
        if (indexDataEdited > -1) {
          Object.assign(data[fieldNameDataCache][indexDataEdited], dataEdited)
        } else {
          throw new Error('No se ha encontrado el registro con el id: ' + id)
        }
      } else {
        // Si no es un array, asigmanos los nuevos valores
        Object.assign(data[fieldNameDataCache], dataEdited)
      }
    }

    proxy.writeQuery({
      query: query,
      variables: variablesQuery,
      data
    })
  }
}

export const update = (params, set, id, query, variablesQuery, calcFieldsOptimisticResponse = {}, dataWriteQueryFunction, refetchQueries) => {
  const { mutationName, typeName } = params

  // Por defecto utilizo OptimisticResponse
  let allowOptimisticResponse = params.hasOwnProperty('allowOptimisticResponse') ? params.allowOptimisticResponse : true

  let options = {
    variables: {
      set: set,
      id: id
    },
    refetchQueries: refetchQueries
  }

  if (allowOptimisticResponse === true ) {
    options.optimisticResponse = {
      __typename: 'Mutation',
      [mutationName]: {
        ...calcFieldsOptimisticResponse,
        __typename: typeName
      }
    }
  }

  // Le añadimos el método update, dependiendo de si hemos pasado un campo fragmentRoot en params,
  // añadimos un update u otro.
  if (params.fragmentRoot) {
    options.update = updateUpdateWithFragment(params, set, id, dataWriteQueryFunction)
  } else {
    options.update = updateUpdateWithQuery(params, set, id, query, variablesQuery, dataWriteQueryFunction)
  }

  return options

}
