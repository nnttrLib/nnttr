import sessionFactory from './sessionFactory'
import mutationsOptionsFactory from './mutationsOptionsFactory'

export default {
  sessionFactory,
  mutationsOptionsFactory
}
