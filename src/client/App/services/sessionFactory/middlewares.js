/* global btoa:false */

import Cookie from 'js-cookie'

import * as actions from './actions'
import * as reducer from './reducers'

// *************************************
// Middleware y utils
// *************************************
export const login = (crypto) => {
  return store => next => (action) => {
    switch (action.type) {
      case actions.t.LOGIN_SUCCESS:
        saveUser(crypto, action)
        break
      case actions.t.LOGIN_LOGOUT:
        removeUser(crypto)
        break
      case actions.t.LOGIN_FAILURE:
        removeUser(crypto)
        break
      default:
    }
    next(action)
  }
}

const removeUser = (crypto, action) => {
  const payload = JSON.stringify(reducer.logoutReducer(undefined, action))
  const payloadEncryptCompress = crypto.encryptCompress(payload)
  window && window.localStorage.setItem(actions.name, payloadEncryptCompress)
  Cookie.remove(actions.name)
}

const saveUser = (crypto, action) => {
  const payload = JSON.stringify(reducer.successReducer(undefined, action))
  const payloadEncryptCompress = crypto.encryptCompress(payload)
  window && window.localStorage.setItem(actions.name, payloadEncryptCompress)
  Cookie.set(actions.name, btoa(payload))
}
