import { createSelector } from 'reselect'

import * as actions from './actions'
import * as reducer from './reducers'

// *************************************
// 5: Definimos los selectores
// *************************************
const sessionSelector = (state) => {
  const sessionState = state[actions.name]
  if (sessionState.isAuthenticated === undefined) return reducer.initialState
  return sessionState
}

// Creamos un Memoized Selector
export const getSession = createSelector(
  sessionSelector,  // Para el resultado del selector, se aplica la función de transformación siguiente:
  (sessionState) => sessionState
)
