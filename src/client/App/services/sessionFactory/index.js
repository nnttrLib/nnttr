import * as actions from './actions'
import * as reducers from './reducers'
import * as selectors from './selectors'
import * as middlewares from './middlewares'
import utils from './utils'

import Crypto from '../../../../utils/Crypto'

export default (secretPassword = 'MiClaveSuperSecreta') => {
  const crypto = new Crypto(secretPassword)
  return {
    name: actions.name,
    initialState: reducers.initialState,
    actions,
    reducers,
    selectors,
    middlewares: {
      login: middlewares.login(crypto),
      removeUser: middlewares.removeUser,
      saveUser: middlewares.saveUser
    },
    utils: utils(crypto)
  }
}
