import Ducks from '../../../../utils/Ducks'

let ducks = new Ducks()

// *************************************
// 1: Nombre del modulo
// *************************************
export const name = 'session'
// *************************************

// *************************************
// 2: Definimos un objeto con los tipos de Acciones
// *************************************
export const t = {
  LOGIN_REQUEST: name + '/' + 'LOGIN_REQUEST',
  LOGIN_FAILURE: name + '/' + 'LOGIN_FAILURE',
  LOGIN_SUCCESS: name + '/' + 'LOGIN_SUCCESS',
  LOGIN_LOGOUT: name + '/' + 'LOGIN_LOGOUT',
  LOGIN_SELECCIONAR_EMPRESA: name + '/' + 'LOGIN_SELECCIONAR_EMPRESA',
  MENU_LEFT_CREATE: name + '/' + 'MENU_LEFT_CREATE',
  MENU_CLICK: name + '/' + 'MENU_CLICK'
}

// *************************************
// 3: Definimos los creadores de Acciones
// *************************************
export const requestLogin = ducks.createAction(t.LOGIN_REQUEST)
export const failureLogin = ducks.createAction(t.LOGIN_FAILURE)
export const successLogin = ducks.createAction(t.LOGIN_SUCCESS)
export const logout = ducks.createAction(t.LOGIN_LOGOUT)
export const seleccionarEmpresa = ducks.createAction(t.LOGIN_SELECCIONAR_EMPRESA)

export const menuLeftCreate = ducks.createAction(t.MENU_LEFT_CREATE)
export const menuClick = ducks.createAction(t.MENU_CLICK)
