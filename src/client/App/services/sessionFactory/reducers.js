import Ducks from '../../../../utils/Ducks'
import * as actions from './actions'

let ducks = new Ducks()

// *************************************
// 4: Definimos el reducer
// *************************************
// Estado Inicial
export const initialState = {
  isLoading: false,
  isAuthenticated: false,
  menuLeft: [],
  menuLeftOpen: [],
  menuLeftSelected: -1
}

export const requestReducer = (state = initialState, action) => {
  const newState = Object.assign({}, state, {
    isLoading: true,
    isAuthenticated: false
  })
  return newState
}

export const failureReducer = (state = initialState, action) => {
  const newState = Object.assign({}, state, {
    isLoading: false,
    isAuthenticated: false,
    error: action.payload
  })
  return newState
}

export const logoutReducer = (state = initialState, action) => {
  return initialState
}

export const successReducer = (state = initialState, action) => {
  let idEmpresaSeleccionada = -1
  let nombreEmpresaSeleccionada = ''
  if (Array.isArray(action.payload.empresasPermitidas) && action.payload.empresasPermitidas.length > 0) {
    idEmpresaSeleccionada = action.payload.empresasPermitidas[0].id_empresa
    nombreEmpresaSeleccionada = action.payload.empresasPermitidas[0].nombre_empresa
  }
  if (typeof idEmpresaSeleccionada === 'string' || idEmpresaSeleccionada instanceof String) {
    idEmpresaSeleccionada = Number(idEmpresaSeleccionada)
  }

  const newState = Object.assign({}, state, {
    isLoading: false,
    isAuthenticated: true,
    idApp: action.payload.idApp,
    usuario: {
      ...action.payload.usuario,
      empresasPermitidas: action.payload.empresasPermitidas,
      idEmpresaSeleccionada,
      nombreEmpresaSeleccionada
    },
    token: action.payload.token,
    tokenFicheros: action.payload.tokenFicheros
  })
  return newState
}

export const seleccionarEmpresaReducer = (state = initialState, action) => {
  const newState = Object.assign({}, state)
  // Cambiamos la empresa seleccionada
  newState.usuario.idEmpresaSeleccionada = action.payload.id_empresa
  newState.usuario.nombreEmpresaSeleccionada = action.payload.nombre_empresa
  return newState
}

let id = 0

function setIdLevelMenu (menu, nivelRoot, idLast, rutaRoot) {
  let nivel = nivelRoot ? nivelRoot + 1 : 1
  id = idLast ? idLast + 1 : 1
  let ruta = rutaRoot || '0'

  for (let i = 0; i < menu.length; i++) {
    menu[i].id = id
    menu[i].nivel = nivel
    menu[i].ruta = ruta + ',' + id
    menu[i].separador = false

    let seccionItems = menu[i].seccionItems
    if (seccionItems && Array.isArray(seccionItems) && seccionItems.length > 0) {
      menu[i].seccion = true
      setIdLevelMenu(seccionItems, nivel, id, menu[i].ruta)
    }

    let subMenu = menu[i].subMenu
    if (subMenu && Array.isArray(subMenu) && subMenu.length > 0) {
      menu[i].dropDown = true
      setIdLevelMenu(subMenu, nivel, id, menu[i].ruta)
    } else {  // Si no tiene submenu miramos a ver si es un separador simplemente
      if (!menu[i].href) {
        menu[i].separador = true
      }
    }
    id = id + 1
  }

  return menu
}

export const menuLeftCreateReducer = (state = initialState, action) => {
  let menu = [...action.payload.slice(0, action.payload.length)]

  const newState = Object.assign({}, state, {
    menuLeft: setIdLevelMenu(menu)
  })

  return newState
}

export const menuClick = (state = initialState, action) => {
  let open = []
  let id = action.payload.id
  let ruta = action.payload.ruta
  let isOpen = state.menuLeftOpen.includes(id.toString())
  if (isOpen) {  // Si el menú está abierto lo tengo que cerrar.
    // Dejo abiertos todos los de la misma ruta excepto el mismo que lo cerramos.
    open = ruta.split(',').filter(item => item !== '0' && item !== id.toString())
  } else { // Si el menú que hemos hecho click no está abierto tenemos que abrirlo
    // Dejamos abiertos todos los de su ruta incluido el mismo.
    open = ruta.split(',').filter(item => item !== '0')
  }

  const newState = Object.assign({}, state, {
    menuLeftOpen: [...open.slice(0, open.length)],
    menuLeftSelected: id
  })
  return newState
}

export const reducer = ducks.createReducer({
  [actions.t.LOGIN_REQUEST]: requestReducer,
  [actions.t.LOGIN_FAILURE]: failureReducer,
  [actions.t.LOGIN_SUCCESS]: successReducer,
  [actions.t.LOGIN_LOGOUT]: logoutReducer,
  [actions.t.LOGIN_SELECCIONAR_EMPRESA]: seleccionarEmpresaReducer,
  [actions.t.MENU_LEFT_CREATE]: menuLeftCreateReducer,
  [actions.t.MENU_CLICK]: menuClick
},
  initialState
)
