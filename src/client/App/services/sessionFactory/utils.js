import * as reducers from './reducers'

const atob = str => Buffer.from(str, 'base64').toString('binary')

const getSessionInfoFromCookie = (req) => {
  const { headers } = req
  if (!headers.cookie) {
    return reducers.initialState
  }
  try {
    const cookie = headers.cookie.split(';').find(c => c.trim().startsWith('session='))
    if (!cookie) {
      return reducers.initialState
    }
    const json = cookie.split('=')[1]
    return JSON.parse(atob(json))
  } catch (parseError) {
    console.error(parseError, headers)
    return {}
  }
}

const getSessionInfoFromLocalStorage = (crypto) => () => {
  const payload = window.localStorage.session
  return payload ? JSON.parse(crypto.decryptCompress(payload)) : {}
}

export default (crypto) => {
  return {
    getSessionInfoFromCookie,
    getSessionInfoFromLocalStorage: getSessionInfoFromLocalStorage(crypto)
  }
}
