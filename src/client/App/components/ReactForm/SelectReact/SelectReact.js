import React from 'react'
import Select from 'react-select'

import Aviso from '../Aviso'

import { FormField } from 'react-form'

class CustomSelectWrapper extends React.Component {
  constructor (props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange (selectedOption) {
    const {
      fieldApi
    } = this.props

    const {
      setValue
    } = fieldApi

    if (selectedOption) {
      setValue(selectedOption.value)
    } else {
      setValue(null)
    }
  }

  render () {
    const {
      fieldApi,
      onInput,
      ...rest
    } = this.props

    const {
      getValue,
      getError,
      getWarning,
      getSuccess,
      setValue,
      setTouched
    } = fieldApi

    const error = getError()
    const warning = getWarning()
    const success = getSuccess()
    const className = '' // para evitar que salga mal cuando se pasa "form-group"

    return (
      <div>
        <Select
          value={getValue()}
          onChange={this.handleChange}
          onInput={(e) => {
            setValue(e.target.value)
            if (onInput) {
              onInput(e)
            }
          }}
          onBlur={() => {
            setTouched()
          }}
          {...rest}
          className={className}
        />
        { error ? <Aviso className='alert alert-danger' mensaje={error} /> : null }
        { !error && warning ? <Aviso className='alert alert-warning' mensaje={warning} /> : null }
        { !error && !warning && success ? <Aviso className='alert alert-success' mensaje={success} /> : null }
      </div>
    )
  }
}

// Use the form field and your custom input together to create your very own input!
const CustomSelect = FormField(CustomSelectWrapper)

export default CustomSelect
