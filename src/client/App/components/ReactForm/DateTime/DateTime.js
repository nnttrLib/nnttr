import React from 'react'
import DateTime from 'react-datetime'
import { FormField } from 'react-form'
import Aviso from '../Aviso'

import moment from 'moment'
moment.locale('es')

class CustomDateTimeWrapper extends React.Component {
  constructor (props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange (selectedDateTime) {
    const {
      fieldApi
      } = this.props

    const {
      setValue
    } = fieldApi

    setValue(selectedDateTime)
  }

  render () {
    const {
      fieldApi,
      onInput,
      ...rest
    } = this.props

    const {
      getValue,
      getError,
      getWarning,
      getSuccess,
      setValue,
      setTouched
    } = fieldApi

    const error = getError()
    const warning = getWarning()
    const success = getSuccess()

    return (
      <div>
        <DateTime
          value={moment(getValue())}
          onChange={this.handleChange}
          onInput={(e) => {
            setValue(e.target.value)
            if (onInput) {
              onInput(e)
            }
          }}
          onBlur={() => {
            setTouched()
          }}
          {...rest} />
        { error ? <Aviso className='alert alert-danger' mensaje={error} /> : null }
        { !error && warning ? <Aviso className='alert alert-warning' mensaje={warning} /> : null }
        { !error && !warning && success ? <Aviso className='alert alert-success' mensaje={success} /> : null }
      </div>
    )
  }
}

// Use the form field and your custom input together to create your very own input!
const CustomDateTime = FormField(CustomDateTimeWrapper)

export default CustomDateTime
