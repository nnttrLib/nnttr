import React from 'react';
import PDF from 'react-pdf-js';

class PdfViewer extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      scale: props.scale || 1
    }
  }

  onDocumentComplete = (pages) => {
    this.setState({ page: 1, pages });
  }

  onPageComplete = (page) => {
    this.setState({ page });
  }

  handlePrevious = () => {
    this.setState({ page: this.state.page - 1 });
  }

  handleNext = () => {
    this.setState({ page: this.state.page + 1 });
  }

  handleAmpliar = () => {
    this.setState({
    scale: this.state.scale + 0.1
  })
  }

  handleReducir = () => {
    this.setState({
    scale: this.state.scale - 0.1
  })
  }


  renderZoom = () => {
    let reducirButton = <li className="previous" onClick={this.handleReducir}><a href='#'><i className='fa fa-search-minus'></i>Reducir</a></li>
    let ampliarButton = <li className="next" onClick={this.handleAmpliar}><a href='#'><i className='fa fa-search-plus'></i>Ampliar</a></li>
    return (
      <nav>
        <ul className="pager">
          {reducirButton}
          {ampliarButton}
        </ul>
      </nav>

    )
  }

renderPagination = (page, pages) => {
    let previousButton = <li className="previous" onClick={this.handlePrevious}><a href="#"><i className="fa fa-arrow-left"></i> Anterior</a></li>;
    if (page === 1) {
      previousButton = <li className="previous disabled"><a href="#"><i className="fa fa-arrow-left"></i> Anterior</a></li>;
    }
    let nextButton = <li className="next" onClick={this.handleNext}><a href="#">Siguiente <i className="fa fa-arrow-right"></i></a></li>;
    if (page === pages) {
      nextButton = <li className="next disabled"><a href="#">Siguiente <i className="fa fa-arrow-right"></i></a></li>;
    }

    return (
      <nav>
        <ul className="pager">
          {previousButton}
          {this.state.page}/{this.state.pages}
          {nextButton}
        </ul>
      </nav>
    );
  }

  render() {
    let pagination = null;
    if (this.state.pages) {
      pagination = this.renderPagination(this.state.page, this.state.pages);
    }
    return (
      <div>
        {pagination}
        <PDF
          file={this.props.file}
          scale={this.state.scale}
          fillWidth={this.props.fillWidth}
          fillHeight={this.props.fillHeight}
          onDocumentComplete={this.onDocumentComplete}
          onPageComplete={this.onPageComplete}
          page={this.state.page}
        />
        {this.renderZoom()}
      </div>
    )
  }
}

export default PdfViewer