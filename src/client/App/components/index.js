import Layout from './Layout'
import Page from './Page'
import ReactTableEnantar from './ReactTableEnantar'
import NavTabs from './NavTabs'
import Select from './Select'
import ConfirmDialog from './ConfirmDialog'
import Modal from './Modal'
import ReactForm from './ReactForm'
import ListTimeLine from './ListTimeLine'
import ReactTableUtils from './ReactTableUtils'
import PageControl from './PageControl'
import InputUploadFiles from './InputUploadFiles'
import PdfViewer from './PdfViewer'

export default {
  Layout,
  Page,
  ReactTableEnantar,
  NavTabs,
  Select,
  ConfirmDialog,
  Modal,
  ReactForm,
  ListTimeLine,
  ReactTableUtils,
  PageControl,
  InputUploadFiles,
  PdfViewer
}
