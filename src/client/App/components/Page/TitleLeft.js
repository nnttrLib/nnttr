const TitleLeft = (props) => {
  return (
    <div className='title_left'>
      {props.children}
    </div>
  )
}

export default TitleLeft
