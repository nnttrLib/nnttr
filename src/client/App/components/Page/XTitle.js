const XTitle = (props) => {
  return (
    <div className='x_title'>
      {props.children}
    </div>
  )
}

export default XTitle
