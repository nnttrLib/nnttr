const XContent = (props) => {
  return (
    <div className='x_content'>
      {props.children}
    </div>
  )
}

export default XContent
