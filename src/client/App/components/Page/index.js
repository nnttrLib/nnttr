import Title from './Title'
import TitleLeft from './TitleLeft'
import XPanel from './XPanel'
import XTitle from './XTitle'
import XContent from './XContent'
import XPanelTitle from './XPanelTitle'

export default {
  Title,
  TitleLeft,
  XPanel,
  XTitle,
  XContent,
  XPanelTitle
}
