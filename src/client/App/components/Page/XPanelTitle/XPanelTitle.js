import React from 'react'

import Titulo from './Titulo.js'
import Contenido from './Contenido.js'

class XPanelTitle extends React.Component {
  constructor (props) {
    super(props)
    this.changeCollapse = this.changeCollapse.bind(this)

    this.state = {
      collapsed: props.collapsed || false
    }
  }

  changeCollapse () {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

  render () {
    const props = this.props

    return (
      <div className={props.className}>
        {props.showTitle &&
        <Titulo
          title={props.title}
          onClickCollapse={this.changeCollapse}
          showCollapseLink={props.showCollapseLink}
          collapsed={this.state.collapsed}
        />
        }
        <Contenido
          collapsed={this.state.collapsed}
        >
          {props.children}
        </Contenido>
      </div>
    )
  }
}

XPanelTitle.defaultProps = {
  className: 'x_panel',
  showTitle: true,
  showCollapseLink: true
//    title:   // Componente a mostrar en el título de la página

}

export default XPanelTitle
