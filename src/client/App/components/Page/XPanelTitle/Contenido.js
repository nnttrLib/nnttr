const Contenido = (props) => {
  const style = {
    display: props.collapsed ? 'none' : 'block'
  }

  return (
    <div className={props.className} style={style}>
      {props.children}
    </div>
  )
}

Contenido.defaultProps = {
  className: 'x_content'
}

export default Contenido
