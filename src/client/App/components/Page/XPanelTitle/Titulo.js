const Titulo = (props) => {
  const classNameCollapseLink = props.collapsed ? 'fa fa-chevron-down' : 'fa fa-chevron-up'
  const hint = props.collapsed ? 'Mostrar Contenido' : 'Ocultar Contenido'
  let title = props.title
  if (typeof title === 'string') {
    title = <b>{title}</b>
  }
  return (
    <div className={props.className}>
      {title &&
      title
      }
      {!title &&
      <span>&nbsp;</span>
      }
      {props.showCollapseLink &&
      <span className='pull-right' onClick={props.onClickCollapse} title={hint}>
        <i className={classNameCollapseLink} />
      </span>
      }

    </div>
  )
}

Titulo.defaultProps = {
  className: 'x_title',
  showCollapseLink: true
}

export default Titulo
