const XPanel = (props) => {
  return (
    <div className='x_panel'>
      {props.children}
    </div>
  )
}

export default XPanel
