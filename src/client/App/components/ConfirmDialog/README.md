# Componente ConfirmDialog

Este componente muestra un modal para que el usuario acepte o no la acción a realizar.

El componente tiene un primer panel con la pregunta que se le realiza al usuario, un segundo panel ( opcional ) con una explicación detallada de lo que supondrá el proceso si lo acepta, y un tercer panel inferior con 2 botones para aceptar o no.

Se puede personalizar tanto la pregunta, como la explicación como los captions de los botones de aceptar o no, así como pasarle las funciones que se lanzarán al hacer click en alguno de los botones.

Los campos obligatorios son la propiedad **pregunta** y la propiedad **handleClickYes**

## Propiedades

|nombre|valor por defecto|descripcion
|---|---|---|
|pregunta|null|Pregunta que se le hace al usuario|
|explicacion|null|Descripción detallada del proceso que se va a realizar|
|handleClickYes|null|Función que se lanza al hacer click en el botón Si|
|handleClickNo|null|Función que se lanza al hacer click en el botón No|
|handleClickClose|null|Función que se lanza al hacer click en la esquina superior derecha para cerrar el modal|
|captionButtonYes|Si|Texto del botón Si|
|captionButtonNo|No|Texto del botón No|

## Ejemplo de uso

En el módulo donde lo utilicemos tenemos que importar la libreria

```javascript
// Importamos la libreria
import nnttr from 'nnttr'
const ConfirmDialog = nnttr.App.components.ConfirmDialog

// en el render tenemos que añadir al final el componente

/* Codigo del comopnente ... */

   render () {
     return (
       <Layaout>
         {/* Otros Comopnentes...  */}
         
         {/* En Algún componente hay que configurar para la activación del modal */}
            <a
              href="#"
               data-toggle="modal"
               data-target="#Eliminar"
               data-keyboard="true"
               data-backdrop="static"
            >
              <u>
                Eliminar
              </u>
            </a>

         {/* Otros Componentes...  */}
                
         <ConfirmDialog
           id="Eliminar"
           pregunta="¿ Quieres eliminar este Registro ?"
           explicacion="El borrado del registro es definitivo, no se podrá recuperar"
           handleClickYes={ this.Eliminar }
           captionButtonYes="Si, eliminarlo"
         />
       </Layaout>
     )
   }


```