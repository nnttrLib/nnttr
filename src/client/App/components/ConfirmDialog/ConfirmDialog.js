import PropTypes from 'prop-types'

const ConfirmDialog = (props) => {
  return (
    <div className='modal fade'
      id={props.id}
      tabIndex='-1'
      role='dialog'
      aria-labelledby='myModalLabel'
    >
      <div className='modal-dialog' role='document'>
        <div className='modal-content'>
          <div className='modal-header'>
            <button type='button' className='close' data-dismiss='modal' aria-label='Close'
              onClick={props.handleClickClose}>
              <span aria-hidden='true'>&times;</span>
            </button>
            <h4 className='modal-title' id='myModalLabel'><b>{props.pregunta}</b></h4>
          </div>
          {props.explicacion &&
            <div className='modal-body' style={{backgroundColor: '#F8F8F9'}}>
              <h5>{props.explicacion} </h5>
            </div>
          }
          <div className='modal-footer'>
            <button type='button' className='btn btn-lg btn-default' data-dismiss='modal'
              onClick={props.handleClickNo}>{props.captionButtonNO}</button>
            <button type='button' className='btn btn-lg btn-primary' data-dismiss='modal'
              onClick={props.handleClickYes}>{props.captionButtonYes}</button>
          </div>
        </div>
      </div>
    </div>
  )
}

ConfirmDialog.propTypes = {
  pregunta: PropTypes.node.isRequired,
  explicacion: PropTypes.node,
  handleClickYes: PropTypes.func.isRequired,
  handleClickNo: PropTypes.func,
  handleClickClose: PropTypes.func

}

ConfirmDialog.defaultProps = {
  captionButtonYes: 'Si',
  captionButtonNO: 'No'
}

export default ConfirmDialog
