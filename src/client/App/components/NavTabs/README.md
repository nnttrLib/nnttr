# Componente NavTabs

Este componente construye una serie de tabs a partir de una array que se le pasa como parámetro.

Puede utilizarse como elemento para aplicar una serie de filtros a una tabla, o como parte de un compentente más completo tipo PageControl.

## Ejemplo de uso

```javascript
// Definición del array de tabs:
    const tabs = [
      {
        caption: 'Prueba 1',
        title: 'Esto es el título',
        active: true,
        onClick: this.handleOnClickTab
        badge: 2,
        titleBadge: 'Estoy Probando',
      },
      {
        caption: <b>Opción 2</b>,
        onClick: this.handleOnClickTab
        badge: 1,
      },
      {
        caption: 'Prueba 3',
        onClick: this.handleOnClickTab
        badge: 15,
      }

    ]

//Uso como componente:

  render () {
    return (
      <div>
        {/* ... Otros comopnentes */}
        <NavTabs tabs={tabs} />
        {/* ... Otros comopnentes */}
      </div>
    )
  }
```