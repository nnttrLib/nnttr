import React from 'react'

class NavTabs extends React.Component {
  constructor (props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)

    // Generamos un id para cada item
    let i = 0
    let tabs = props.tabs.map(item => {
      return {
        _id: i++,
        ...item
      }
    })

    // Buscamos el que está seleccionado
    let itemSelected = tabs.find(item => item.active === true)
    let tabSelected = itemSelected ? itemSelected._id : 0

    this.state = {
      tabSelected,
      tabs
    }
  }

  handleClick (itemTab) {
    // Primero seleccionamos el tab
    this.setState({
      tabSelected: itemTab._id
    })
    // Luego Ejecutamos el onClick del Tab
    if (itemTab.onClick && typeof itemTab.onClick === 'function') {
      itemTab.onClick(itemTab)
    }
  }

  render () {
    const props = this.props
    const id = props.id || 'MyTab-' + Math.floor((Math.random() * 1000) + 1)
    return (
      <ul id={id} className={props.className} role='tablist'>
        {this.state.tabs.map(item =>
          <li
            key={item._id}
            role='presentation'
            className={item._id === this.state.tabSelected ? 'active ' : ''}
            title={item.title}
            onClick={() => this.handleClick(item)}
          >
            <a href={item.href || '#'}
              id={item._id}
              role='tab'
              data-toggle='tab'
              aria-expanded='true'>
              {item.caption}
              {item.badge &&
                <span>&nbsp;</span>
              }
              {item.badge &&
              <span className='badge pull-right' title={item.titleBadge}>
                {item.badge}
              </span>
              }

            </a>
          </li>
        )}
      </ul>
    )
  }
}

NavTabs.defaultProps = {
  className: 'nav nav-tabs bar_tabs',
  tabs: []
}

export default NavTabs
