import moment from 'moment'

moment.locale('es')

export const date = (cell) => {
  let fromNow
  let format
  if (cell.value === null) {
    fromNow = ''
    format = ''
  } else {
    fromNow = moment(cell.value).fromNow()
    format = moment(cell.value).format('DD/MM/YYYY')
  }
  return <span title={fromNow}>{format}</span>
}

export const dateTime = (cell) => {
  let fromNow
  let format
  if (cell.value === null) {
    fromNow = ''
    format = ''
  } else {
    fromNow = moment(cell.value).fromNow()
    format = moment(cell.value).format('DD/MM/YYYY HH:mm')
  }
  return <span title={fromNow}>{format}</span>
}
