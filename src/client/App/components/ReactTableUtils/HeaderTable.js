import PropTypes from 'prop-types'

import utils from '../../../../utils'

import ConfirmDialog from '../ConfirmDialog'

const RightComponentDefault = (props) => {
  return (
    <div>
      {props.selection.length > 0 && props.showRemoveButton &&
      <button
        type='button'
        className='mb-4 btn-xs btn-danger pull-left glyphicon glyphicon-remove'
        data-toggle='modal'
        data-target={props.dataTargetEliminar}
        data-keyboard='true'
        data-backdrop='static'
        title='Eliminar Registros Seleccionados'
      >
        &nbsp;({props.selection.length})
      </button>
      }

      {props.selection.length === 0 && props.showAddButton &&
        <button
          type='button'
          className='btn-xs btn-default pull-right glyphicon glyphicon-plus'
          data-toggle='modal'
          data-target={props.dataTarget}
          data-keyboard='true'
          data-backdrop='static'
          onClick={props.onCreate}
          title={props.titleNew}
        >
          {props.captionNew &&
          <span>&nbsp;{props.captionNew}</span>
          }
        </button>
      }

      {props.selection.length === 1 && props.showEditButton &&
      <button
        className='mb-4 btn-xs btn-default pull-right glyphicon glyphicon-pencil'
        data-toggle='modal'
        data-target={props.dataTarget}
        data-keyboard='true'
        data-backdrop='static'
        onClick={() => props.onEdit(props.selection[0])}
        title={props.titleEdit}
      >
        {props.captionEdit &&
        <span>&nbsp;{props.captionEdit}</span>
        }

      </button>
      }

      {props.showRefreshButton &&
      <button
        className='mb-4 btn-xs btn-default pull-right glyphicon glyphicon-repeat'
        onClick={() => props.onRefresh && typeof props.onRefresh === 'function' ? props.onRefresh() : console.log('ERROR: no se ha actualizado, hay que pasar un evento onRefresh') }
        title={props.titleRefresh}
      >
        {props.captionRefresh &&
        <span>&nbsp;{props.captionRefresh}</span>
        }

      </button>
      }
    </div>
  )
}

const HeaderTable = (props) => {
  let pregunta = '¿ Quieres eliminar este Registro ?'
  let explicacion = 'El borrado del registro es definitivo, no se podrá recuperar'
  let caption = 'Si, eliminarlo'
  if (props.selection.length > 1) {
    pregunta = '¿ Quieres eliminar los ' + props.selection.length + ' Registros Seleccionados ?'
    explicacion = 'El borrado de los ' + props.selection.length + ' registros será definitivo, no se podrán recuperar'
    caption = 'Si, eliminarlos'
  }

  const LeftComponent = props.LeftComponent || null
  const RightComponent = props.RightComponent || RightComponentDefault

  let colLeft = 12 - props.colRight
  const classNameColLeft = 'col-md-' + colLeft + ' col-sm-' + colLeft + ' col-xs-12'
  const classNameColRight = 'col-md-' + props.colRight + ' col-sm-' + props.colRight + ' col-xs-12'

  const idEliminar = utils.helpers.getRandomInt(1, 10000)
  const dataTargetEliminar = 'Eliminar-' + idEliminar

  return (
    <div className='panel' >
      <div className='row' style={{ display: 'flex', alignItems: 'center' }}>
        <div className={classNameColLeft + ' pull-left'}>
          {LeftComponent &&
            <LeftComponent {...props} />
          }
        </div>
        {props.colRight > 0 &&
          <div className={classNameColRight + ' pull-right'}>
            {props.showRightComponent &&
            <RightComponent
              {...props}
              dataTargetEliminar={'#' + dataTargetEliminar}
            />
            }

          </div>
        }
        {props.selection.length > 0 &&
          <ConfirmDialog
            id={dataTargetEliminar}
            pregunta={pregunta}
            explicacion={explicacion}
            handleClickYes={() => props.onDelete(props.selection)}
            captionButtonYes={caption}
          />
        }

      </div>
    </div>
  )
}

HeaderTable.propTypes = {
  selection: PropTypes.array.isRequired,
  dataTarget: PropTypes.string,

  onCreate: PropTypes.func,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  onRefresh: PropTypes.func,

  captionNew: PropTypes.node,
  captionEdit: PropTypes.node,
  captionRefresh: PropTypes.node,
  titleNew: PropTypes.string,
  titleEdit: PropTypes.string,
  titleRefresh: PropTypes.string
}

HeaderTable.defaultProps = {
  selection: [],
  showRightComponent: true,
  titleNew: 'Añadir',
  titleEdit: 'Modificar',
  titleRefresh: 'Actualizar',
  colRight: 2,
  showEditButton: true,
  showAddButton: true,
  showRemoveButton: true,
  showRefreshButton: false,
}

export default HeaderTable
