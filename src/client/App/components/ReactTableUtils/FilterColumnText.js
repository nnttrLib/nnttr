// Componente para personalizar el campo de filtro de la columna
const FilterColumnText = (props) => {
  const filter = props.filter
  const style = {
    width: '100%'
  }
  return (
    <input
      title={props.title}
      type={props.type}
      placeholder={props.placeholder}
      value={filter ? filter.value : ''}
      onChange={event => props.onChange(event.target.value)}
      style={style}
    />
  )
}

FilterColumnText.defaultProps = {
  title: 'Escribir para buscar y/o filtrar',
  type: 'text'
}

export default FilterColumnText
