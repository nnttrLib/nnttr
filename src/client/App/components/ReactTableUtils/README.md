# Componentes y utilidades para usar con ReactTableEnantar

## FilterColumn
Componente para personalizar el campo que se utiliza para el filtro de una columna.
Con él podemos pasar un title o un placeholder

Ejemplo de uso en la definición de una columna:

```javascript

import nnttr from 'nnttr'
const FilterColumnText  = nnttr.App.components.ReactTableUtils.FilterColumnText

// ...

  const columns = [
    {
      accessor: 'DESCRIPCION2',
      style: {
        whiteSpace: "pre-wrap",
      },
      Cell:  (cell) => <LineaDetalle linea={cell.original} />,
      filterMethod: filtroDescripcion,
      Filter: (props) => <FilterColumnText placeholder="Escribe algo para busar" title="Filtrar y buscar" {...props} />
    }
    ]
```