---
title: PageControl
description: Para crear un PageControl con múltiples solapas donde mostrar en cada una un componente diferente 
categoria: client.App.components 
---

Componente que nos facilita la creación de un PageControl al estilo del que se utiliza en Delphi.
Esquema básico del html que genera el componente:
```html
<div>
       <!-- Bloque que se repite por cada solapa !-->
       <ul className={props.classNameButtons}>
            <li className="active">
              <a href={"#Etiqueta"} data-toggle="tab">Etiqueta</a>
            </li>

       <!-- Bloque que se repite por cada solapa !-->
        <div className={props.classNameContents}>
          <div className={props.classNameContent} id={"etiqueta"}>
            <p/>
            {props.component}
          </div>

        </div>
</div>

```

## Uso del Componente

El componente está dentro de App, para usarlo podemos hacer lo siguiente:

```javascript
        <App.components.PageControl
            titulo="Titulo del PageControl"
            tabSheets={tabSheets}
        />
    )
```

Sólo hay una propiedad obligatoria, `tabSheets` que debe ser un **array** de objetos, donde cada objeto describe cada una de las **solapas** del PageControl
La propiedad `titulo` sólo se mostrará si se pasa algo, sino no se renderiza. 

Por cada objeto **tabSheet** tenemos que pasar un **caption** con la etiqueta de la solapa y un componente que se renderizará dentro.
También se puede indicar en una de las solapas si va a estar activa por defecto.

Ejemplo de una configuración de un PageControl:

```javascript
 var tabSheets = [
        {
           caption: 'Etiqueta 1',  // La etiqueta de la solapa
           active: true, // Solapa activa por defecto
           component: <MiComponente1 />  // Componente que se "renderizará" en esta solapa 
        },
        {
           caption: 'Etiqueta 2',  // La etiqueta de la solapa
           component: <MiComponente2 />  // Componente que se "renderizará" en esta solapa 
        },
        {
           caption: 'Etiqueta 3',  // La etiqueta de la solapa
           component: <MiComponente3 />  // Componente que se "renderizará" en esta solapa 
        },
    }    
```

## Api

| **Propiedad**| **Tipo**| **Valor por defecto**| **Descripción**|
| :------- | :------: | :----- |---|
|**tabSheets**|array||array de objetos, cada uno de ellos representa una solapa del PageControl|
|titulo|string||Titulo del PageControl|
|classNameContainer|string|'container'|  |
|classNameTitulo|string|'panel-heading text-center'|Permite personalizar el estilo del título|
|classNamePanel|string|'panel panel-default'||
|classNameButtons|string|'nav nav-pills'|Permite personalizar el estilo de las etiquetas de las solapas|
|classNameContents|string|'tab-content clearfix'||
|classNameContent|string|'tab-pane'||

## Información de referencia

[bootstrap](http://getbootstrap.com/components/#nav-pills)
[w3schools.com](http://www.w3schools.com/bootstrap/bootstrap_tabs_pills.asp)
