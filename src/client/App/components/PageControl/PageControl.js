import React from 'react'
import PropTypes from 'prop-types'

const allTrim = (str) => str.replace(/ /g, '')

const TabSheetButton = (props) => {
  var etiqueta = allTrim(props.caption)
  return (
    <li role='presentation' className={props.active ? 'active' : ''}>
      <a href={'#' + etiqueta} data-toggle='tab' role='tab' aria-expanded='true'>
        {props.caption}
                &nbsp;
        {props.badge &&
        <span className='badge pull-right'>
          {props.badge}
        </span>
                }
      </a>
    </li>
  )
}

const TabSheetsButtons = (props) => {
  return (
    <ul key='Buttons' className={props.classNameButtons} role='tablist'>
      {
             props.tabSheets.map(tabSheet => <TabSheetButton key={allTrim(tabSheet.caption)} {...tabSheet} />)
            }
    </ul>
  )
}

const TabSheetContent = (props) => {
  let etiqueta = allTrim(props.caption)

  return (
    <div className={props.active ? props.classNameContent + ' active in' : props.classNameContent} id={etiqueta}>
      <p />
      {props.component}
    </div>
  )
}

const TabSheetsContents = (props) => {
  return (
    <div className={props.classNameContents}>
      {
             props.tabSheets.map(tabSheet => <TabSheetContent key={allTrim(tabSheet.caption)}
               classNameContent={props.classNameContent}
               {...tabSheet}
                                              />)
            }
    </div>
  )
}

const TabSheets = (props) => {
  return (
    <div role='tabpanel' data-example-id='togglable-tabs'>
      <TabSheetsButtons classNameButtons={props.classNameButtons}
        tabSheets={props.tabSheets}
         />
      <TabSheetsContents classNameContents={props.classNameContents}
        classNameContent={props.classNameContent}
        tabSheets={props.tabSheets}
         />
    </div>
  )
}

class PageControl extends React.Component {
  constructor (props) {
    super(props)
    this.changeCollapse = this.changeCollapse.bind(this)

    this.state = {
      collapsed: props.collapsed || false
    }
  }

  changeCollapse () {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

  render () {
    const props = this.props
    const style = {
      display: this.state.collapsed ? 'none' : 'block'
    }
    return (
      <div className='x_content' style={style}>
        {props.tabSheets.length === 0 ? <p>Hay que pasar un array con la definición de las solapas a mostrar</p>
                    : props.tabSheets.length === 1 ? props.tabSheets[0].component
                        : <TabSheets classNameButtons={props.classNameButtons}
                          classNameContents={props.classNameContents}
                          classNameContent={props.classNameContent}
                          tabSheets={props.tabSheets}
                        />

                }
      </div>
    )
  }
}

PageControl.propTypes = {
  tabSheets: PropTypes.array.isRequired,
  id: PropTypes.string,
  titulo: PropTypes.string,
  classNameContainer: PropTypes.string.isRequired,
  classNamePanel: PropTypes.string.isRequired,
  classNameTitulo: PropTypes.string.isRequired,
  classNameButtons: PropTypes.string.isRequired,
  classNameContents: PropTypes.string.isRequired,
  classNameContent: PropTypes.string.isRequired
}

PageControl.defaultProps = {
  id: 'PageControl-' + Math.floor((Math.random() * 1000) + 1), // Nº Aleatorio entre 1 y 1000
  tabSheets: [],
  classNameContainer: 'container',
  classNameTitulo: 'panel-heading text-center',
  classNamePanel: 'panel panel-default',
  classNameButtons: 'nav nav-tabs bar_tabs',
  classNameContents: 'tab-content',
  classNameContent: 'tab-pane fade',
  mostrarCollapseLink: true
}

export default PageControl
