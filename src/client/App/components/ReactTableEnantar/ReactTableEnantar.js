import React from 'react'
import PropTypes from 'prop-types'
import ReactTable from 'react-table'
import checkboxHOC from 'react-table/lib/hoc/selectTable'
import crypto from 'crypto'

// Le añadimos la capacidad de seleccionar registros al componente original
const CheckboxTable = checkboxHOC(ReactTable)

// Nuevo componente que cambiará los textos a español y añadirá funcionalidad
class ReactTableEnantar extends React.Component {
  constructor (props) {
    super(props)
    this.dataWithId = this.dataWithId.bind(this)
    this.toggleSelection = this.toggleSelection.bind(this)
    this.toggleAll = this.toggleAll.bind(this)
    this.isSelected = this.isSelected.bind(this)
    this.getSelection = this.getSelection.bind(this)

    // Campos keyField indica el campo que actúa como clave primaria
    // SOLO puede ser un campo la clave primaria, no puede ser la combinación de varios campos
    // si no tiene clave primaria definida, o no es de un solo campo es mejor no pasar esta
    // propiedad al componente, si no existe esta propiedad el componente creará automáticamente
    // una propiedad _id con un valor aleatoria a modo de clave primaria
    // para poder gestioanr la selección de los registros.
    const keyField = props.keyField
    // en data copiamos el array original, y en función del valor del keyField se le añadirá el campo _id o no
    const data = this.dataWithId(props.data, keyField)

    // En el estado guardamos los datos originales: data, y un objeto dataById que es lo mismo que data
    // pero en lugar de un array, es un objeto con una propiedad por cada valor del campo que se indique en keyField  que
    // será la clave primaria, o el valor aleatoria _id que se haya generado
    // de esa forma tenemos un acceso directo a los registros seleccionados.
    this.state = {
      data: data,
      dataById: this.dataById(data, keyField), // Construimos un objeto con una propiedad con clave primaria ( si no se pasa keyField se crea una propidad _id) por cada objeto del array
      selection: this.props.selection || [],
      selectAll: this.props.selectAll || false
    }
  }

  // Cuando se pasan nuevos datos, hay que volver a construir el estado del componente
  // para que se vuelva a renderizar con la nueva información.
  componentWillReceiveProps (nextProps) {
    const keyField = nextProps.keyField
    const data = this.dataWithId(nextProps.data, nextProps.keyField)
    this.setState({
      data,
      dataById: this.dataById(data, keyField),
      selection: nextProps.selection || [],
      selectAll: nextProps.selectAll || false
    })
  }

  // Función que nos seleccion o des-selecciona un registro
  toggleSelection (key, shift, row) {
    // Hacemos una copia del array de registros seleccionados
    let selection = [
      ...this.state.selection
    ]

    const keyIndex = selection.indexOf(key)
    // Chequeamos si ya tenemos ese valor entre los seleccionamos
    if (keyIndex >= 0) { // Si existe lo eliminamos del array
      selection = [
        ...selection.slice(0, keyIndex),
        ...selection.slice(keyIndex + 1)
      ]
    } else { // Si no existe lo añadimos
      selection.push(key)
    }

    if (this.props.handleToggleSelection) {
      this.props.handleToggleSelection({
        selection: this.getSelection(selection)
      })
    } else {
      // Actualizamos el estado
      this.setState({
        selection
      })
    }
  }

  // Función que selecciona TODOS los registros, de todas las páginas
  // seleccionará los registros filtrados y ordenados de ReactTable
  // si hay varias páginas también se seleccionarán los registros que NO se están viendo en la página actual
  toggleAll () {
    const props = this.props
    const selectAll = !this.state.selectAll
    const selection = []
    if (selectAll) {
      // Necesitamos obtener la instancia interna de ReactTable
      const wrappedInstance = this.checkBoxTable.getWrappedInstance()
      // La propiedad 'sortedData' contiene los registros actualmente accesibles basados en el filtro y la ordenación
      const currentRecords = wrappedInstance.getResolvedState().sortedData
      // Añadimos los IDs al array de seleccion
      currentRecords.forEach(item => {
        selection.push(item._original[props.keyField])
      })
    }
    this.setState({
      selectAll,
      selection
    })
    if (this.props.handleToggleSelection) {
      this.props.handleToggleSelection({
        selectAll,
        selection: this.getSelection(selection)
      })
    }
  }

  // Función que nos dice si una clave está incluida en el array de selección o no
  isSelected (key) {
    return this.state.selection.includes(key)
  }

  // Si no hay clave primaria añade un campo a los registros con un valor aleatoria a modo de clave primaria
  dataWithId (data, keyField) {
    let dataWithId
    if (keyField === '_id') {
      dataWithId = data.map(item => {
        const _id = crypto.randomBytes(16).toString('hex')
        return {
          _id,
          ...item
        }
      })
    } else {
      dataWithId = data
    }

    return dataWithId
  }

  // A partir de data ( un array de objetos ) crea un objeto con tantas propiedades como elementos del array, y como valor de la propiedad la clave primaria indicada en keyField
  dataById (data, keyField = '_id') {
    let byId = {}

    data.map(item => {
      byId[item[keyField]] = item
    })

    return byId
  }

  // Devuelve un array de objetos con los registros seleccionados, si se hubiera creado la propiedad _id
  // aquí se elimina para devolver al exterior del componente la misma estructura de datos
  getSelection (selection) {
    const selectedItems = selection || this.state.selection
    return selectedItems.map(item => {
      let registro
      if (this.props.keyField === '_id') {
        registro = Object.assign({}, this.state.dataById[item])
        // Borramos la propiedad _id que hemos creado de forma automática
        // para que fuera de este componente el registro de datos tenga
        // los campos que tiene que tener.
        if (registro.hasOwnProperty('_id')) {
          delete registro._id
        }
      } else {
        registro = this.state.dataById[item]
      }
      return registro
    })
  }

  render () {
    const props = this.props

    // Por defecto se mostrará el componente ReactTable
    let ReactTableComponent = ReactTable
    // Por defecto no se definen propiedades para la opción de seleccionar registros
    let CheckBoxProps = {}

    // Si queremos que nuestro comopnente permita seleccionar registros, configuramos las propiedades necesarias
    // Y cambiamos el componente amostrar por Checkboxtable
    if (props.allowSelect) {
      CheckBoxProps = {
        selectAll: this.state.selectAll,
        isSelected: this.isSelected,
        toggleSelection: this.toggleSelection,
        toggleAll: this.toggleAll,
        selectType: 'checkbox'
      }

      // El comopnente a mostrar ahora será CheckboxTable
      ReactTableComponent = CheckboxTable
    }

    // Preparamos las propiedades que nos interesa pasar a los componetes Header/footer
    const headerFooterProps = {
      loading: props.loading,
      selection: this.getSelection(),
      selectAll: this.state.selectAll,
      columns: props.columns,
      data: props.data
    }

    return (
      <div>
        {/* Si queremos mostrar un componente como cabecera activamos la propiedad showHeader y pasamos el componente */}
        {!props.loading && props.showHeader && props.headerComponent &&
          <props.headerComponent {...headerFooterProps} />
        }

        {/* Renderizamos react-table, bien el componente original o bien el HOC select para poder seleccionar registros */}
        {/* necesitamos definir ref para luego en toggleAlls poder acceder a getWrappedInstance */}
        <ReactTableComponent
          ref={(r) => { this.checkBoxTable = r }}
          {...props}
          data={this.state.data}
          keyField={props.keyField}
          {...CheckBoxProps}
        >
          {props.children}
        </ReactTableComponent>

        {/* Si queremos mostrar un comopnente como pie activamos la propiedad showFooter y pasamos un comopnente */}
        {!props.loading && props.showFooter && props.footerComponent &&
          <props.footerComponent {...headerFooterProps} />
        }

      </div>
    )
  }
}

ReactTableEnantar.propTypes = {
  headerComponent: PropTypes.func,
  footerComponent: PropTypes.func,
  keyField: PropTypes.string,

  // Para poder tener acceso a los elementos seleccionados desde fuera del componente
  // le podremos pasar una función que se ejecutará cada vez que se seleccione un elemento
  // y a la qu se le pasará por parámetros un objeto con las propiedades selection y selectAll
  // para que así en el elemento padre se pueda tener acceso a esa información.
  handleToggleSelection: PropTypes.func,
  // Si en el padre tenemos los valores selection y selectAll, podremos pasárselos como props
  // a la tabla para ajustar y controlar esa selección desde fuera del componente.
  selection: PropTypes.array,
  selectAll: PropTypes.bool
}

ReactTableEnantar.defaultProps = {
  showPagination: true,
  className: '-striped -highlight',
  previousText: 'Anterior',
  nextText: 'Siguiente',
  loadingText: 'Cargando datos, espere un momento...',
  noDataText: 'No hay registros',
  pageText: 'Página',
  ofText: 'de',
  rowsText: 'registros',

  showHeader: true,
  showFooter: true,
  allowSelect: true,
  keyField: '_id'

//  selection: [],
//  selectAll: false
}

export default ReactTableEnantar
