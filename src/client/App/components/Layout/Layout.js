import React from 'react'
import PropTypes from 'prop-types'

import SeleccionarEmpresa from './SeleccionarEmpresa'

const handleOnClick = () => {
  document.body.classList.toggle('nav-md')
  document.body.classList.toggle('nav-sm')
}

class Layout extends React.Component {
  constructor (props) {
    super(props)
    this.handleSeleccionarEmpresa = this.handleSeleccionarEmpresa.bind(this)
  }

  handleSeleccionarEmpresa (nombreEmpresaSeleccionada) {
    const props = this.props
    let empresa = props.session.usuario.empresasPermitidas.filter(item => item.nombre_empresa === nombreEmpresaSeleccionada)
    if (empresa && Array.isArray(empresa) && empresa.length > 0) {
      if (typeof props.actions.session.seleccionarEmpresa === 'function') {
        props.actions.session.seleccionarEmpresa(empresa[0])
      }
    }
  }

  render () {
    const props = this.props

    if (!props.showSideBar) { // Si no mostramos el sidebar lo colapsamos.
      handleOnClick()
    }

    let optionsSelect = []
    optionsSelect = props.session.usuario.empresasPermitidas.map(item => item.nombre_empresa)

    return (
      <div className='container body'>
        <div className='main_container'>
          {/* Panel izquierdo */}
          {props.showSideBar &&
          <div className='col-md-3 left_col'>
            <div className='left_col scroll-view'>
              {/* titulo del panel de la izquierda */}
              <div className='navbar nav_title'>
                {props.SiteTitle &&
                <props.SiteTitle
                  logoApp={props.logoApp} name={props.name} session={props.session}
                  actions={props.actions} />
                }
              </div>
              {/* Zona de menús principal */}
              <div id='sidebar-menu' className='main_menu_side hidden-print main_menu'>
                {props.MainMenu &&
                <props.MainMenu session={props.session} actions={props.actions} />
                }
              </div>
              {/* zona de menú sidebar inferiro */}
              <div className='sidebar-footer hidden-small'>
                {props.FooterSideBar &&
                <props.FooterSideBar session={props.session} actions={props.actions} />
                }
              </div>
            </div>
          </div>
          }
          {/* barra superior */}
          <div className='top_nav'>
            <div className='nav_menu'>
              {/* barra navegación superior */}
              <nav>
                {/* Botón para colapsar panel izquierdo */}
                {props.showSideBar &&
                <div className='nav toggle' onClick={handleOnClick}>
                  <a id='menu_toggle'>
                    <i className='fa fa-bars' />
                  </a>
                </div>
                }

                {/* Selección de Empresa Alineada a la izquierda */}
                {props.session.usuario.multiEmpresa &&
                <ul className='nav navbar-nav navbar-left'>
                  <li>
                    <a className='navbar-brand'
                      title='Haz click para cambiar de empresa'
                      data-toggle='modal'
                      data-target='#SeleccionarEmpresaModal'
                      data-keyboard='true'
                      data-backdrop='static'
                    >{props.session.usuario.nombreEmpresaSeleccionada.substring(0, 15)}
                    </a>
                  </li>
                </ul>
                }

                {/* Opciones Menús alineados a la derecha */}
                <ul className='nav navbar-nav navbar-right'>
                  {/* Opción de menú que aparecerá más a la derecha, Menú Personalizado para el Usuario */}
                  {props.showUserProfileMenu && props.UserProfileMenu &&
                  <props.UserProfileMenu session={props.session} actions={props.actions} />
                  }

                  {/* Barra de Menús normal */}
                  {props.TopMenu &&
                  <props.TopMenu session={props.session} actions={props.actions} />
                  }
                </ul>
              </nav>
            </div>
          </div>
          {/* Page Content */}
          <div className='right_col' role='main'>
            {props.children}
          </div>
          {props.showFooter &&
          <footer>
            <div className='pull-right'>
              {props.footer}
            </div>
            <div className='clearfix' />
          </footer>
          }
        </div>

        <SeleccionarEmpresa
          titulo='Selección de Empresa'
          optionsSelect={optionsSelect}
          empresaActual={props.session.usuario.nombreEmpresaSeleccionada}
          onButtonClick={this.handleSeleccionarEmpresa}
        />

      </div>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.node
}

Layout.defaultProps = {
  name: 'Plantilla',
  showSideBar: true,
  showFooter: true,
  showUserProfileMenu: true,
  footer: 'Plantilla Desarrollo Aplicaciones Web'
}

Layout.displayName = 'Layout nnttr'

export default Layout
