import React from 'react'

import Select from '../../Select'

class SeleccionarEmpresa extends React.Component {
  constructor (props) {
    super(props)
    if (props.empresaActual) {
      this.state = {
        selectValue: props.empresaActual
      }
    } else {
      this.state = {
        selectValue: null
      }
    }
    this.updateValue = this.updateValue.bind(this)
    this.onButtonClick = this.onButtonClick.bind(this)
  }
  updateValue (newValue) {
    this.setState({
      selectValue: newValue
    })
  }
  onButtonClick () {
    this.props.onButtonClick(this.state.selectValue)
  }

  render () {
    return (
      <div className='modal fade' id='SeleccionarEmpresaModal' tabIndex='-1' role='dialog' aria-labelledby='myModalLabel' >
        <div className='modal-dialog' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <button type='button' className='close' data-dismiss='modal' aria-label='Close' >
                <span aria-hidden='true'>&times;</span>
              </button>
              <h4 className='modal-title' id='myModalLabel'>{this.props.titulo}</h4>
            </div>
            <div className='modal-body' >
              <Select
                name='form-field-name'
                autofocus
                placeholder='Seleccionar...'
                handleOnChange={this.updateValue}
                selectedOption={this.state.selectValue}
                options={this.props.optionsSelect}
              />
            </div>
            <div className='modal-footer'>
              <button type='button'
                className='btn btn-lg btn-primary'
                data-dismiss='modal'
                disabled={this.state.selectValue === null}
                onClick={this.onButtonClick}

              >Seleccionar</button>
            </div>
          </div>
        </div>
      </div>

    )
  }
}

export default SeleccionarEmpresa
