// Importamos la librería necesaria para React
import React from 'react'
import Page from '../Page'

import Lista from './Panel/Lista/Lista.js'

class ListTimeLine extends React.Component {
  render () {
    const props = this.props
    return (
      <Page.XPanelTitle title={this.props.title}>
        <Lista
          {...props}
        />
      </Page.XPanelTitle>
    )
  }
}

ListTimeLine.defaultProps = {
  filterable: false,
  showHeader: false,
  height: '500px',
  minRows: '4',
  allowSelect: false,
  showPagination: false,
  className: 'list-unstyled timeline',
  headerColumn: 'Descripción'
}

export default ListTimeLine
