---
title: ListTimeLine
description: Muestra un TimeLine de la información suministrada
---

Este componente se utiliza para mostrar visualmente la evolución a lo largo del tiempo de una serie de sucesos.
Se le pasa como parámetro un array de elementos que será lo que se muestre visualmente.
Se utiliza, por ejemplo, para mostrar el detalle de las versiones publicadas de una aplicación.

## Uso del Componente

```javascript
      <ListTimeLine
        title="Descripción del bloque, puede ser un componente"
        lineas={lineas}
      />
```

Cada elemento del array lineas tendrá la estructura:

```javascript
{
    etiqueta,
    titulo,
    subTitulo,
    descripcion,
    textoBusqueda
}
```

La propiedad `descripcion` puede venir en formato **markdown** para que la presentación sea más visual.
La propiedad textoBusqueda se puede utilizar para unir varios campos, si tiene algo el filtro se aplica a este campo

## Api
| **Propiedad**| **Tipo**| **Valor por defecto**| **Descripción**|
| :------- | :------: | :----- |---|
|**filterable**|boolean|false|muestra un campo para filtrar las lineas|
|**showHeader**|boolean|true|muestra la cabecera de la columna|
|**height**|string|'500px'|altura del componente, hará scroll si hay muchas lineas|
|**minRows**|string|'4'|Nº mínimo de filas que se muestra|
|**allowSelect**|boolean|false|Activa o desactiva la posibilidad de seleccionar una fila|
|**showPagination**|boolean|false|muestra o no la paginación|
|**className**|string|'list-unstyled timeline'|className que se le pasará al componente ReactTableEnantar|
|**sorted**|array|null|Array de objetos indicando la ordenación de ReactTableEnantar|



