const Panel = (props) => (
  <div className='x_panel'>
    {props.children}
  </div>
)

export default Panel
