import ReactTableEnantar from '../../../ReactTableEnantar'
import Linea from './Linea/Linea.js'

function filtrarCadenaCaracteres (filter, row) {
  // Si hemos pasado un texto de busqueda buscamos por ahí
  // sino usamos el campo que se haya pasado.
  const textoBusqueda = row._original.textoBusqueda || row[filter.id]
  return textoBusqueda.toUpperCase().includes(filter.value.toUpperCase())
}

const Lista = (props) => {
  const placeholderFilter = props.placeholderFilter
  // Componente para personalizar el campo de filtro de la columna
  const Filtro = (props) => {
    const filter = props.filter
    const style = {
      width: '100%'
    }
    return (
      <input
        title='Escribe algo para buscar/filtrar'
        type='text'
        placeholder={placeholderFilter}
        value={filter ? filter.value : ''}
        onChange={event => props.onChange(event.target.value)}
        style={style}
      />
    )
  }

  let className = props.className
  if (props.lineas && Array.isArray(props.lineas) && props.lineas.length > 0 && props.lineas[0].etiqueta === undefined) {
    className = className + ' widget'
  }

  const columns = [
    {
      Header: props.headerColumn,
      accessor: 'descripcion',
      filterMethod: filtrarCadenaCaracteres,
      Filter: props.Filter || Filtro,
      Cell: (cell) => <Linea linea={cell.original} />
    }
  ]

  if (!props.showHeader) {
    delete columns[0].Header
  }

  return (
    <ReactTableEnantar
      loading={props.loading}
      filterable={props.filterable}
      className={className}
      minRows={props.minRows}
      sorted={props.sorted}
      data={props.lineas}
      columns={columns}
      pageSize={props.lineas.length}
      style={{
        height: props.height // This will force the table body to overflow and scroll, since there is not enough room
      }}
      allowSelect={props.allowSelect}
      showPagination={props.showPagination}
      />
  )
}

export default Lista
