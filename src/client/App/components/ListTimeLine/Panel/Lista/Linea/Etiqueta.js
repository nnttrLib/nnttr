function paddingRight (s, c, n) {
  if (!s || !c || s.length >= n) {
    return s
  }
  let max = (n - s.length) / c.length
  for (let i = 0; i < max; i++) {
    s += c
  }
  return s
}

const Etiqueta = (props) => {
  var etiqueta = props.etiqueta
  if (typeof props.etiqueta === 'string') {
    // Si no añadimos el espacio en blanco considera una palabra y la saca en 2 líneas
    etiqueta = props.etiqueta.substring(0, 19) + ' '
    // Rellenamos a caracteres para que visualmente quede bonito
    etiqueta = paddingRight(etiqueta, '\u00a0', 20)
  }
  // Bajamos el tamaño de la fuente para que si es muy grande se vea en 2 líneas bien
  var estiloLinea = {
    fontSize: '10px'
  }
  return (
    <div className='tags'>
      <span className='tag' style={estiloLinea}>{etiqueta}</span>
    </div>
  )
}

export default Etiqueta
