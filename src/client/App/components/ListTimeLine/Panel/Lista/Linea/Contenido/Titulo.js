const Titulo = (props) => {
  return (
    <h2 className='title'>
      <b>{props.titulo}</b>
    </h2>

  )
}

export default Titulo
