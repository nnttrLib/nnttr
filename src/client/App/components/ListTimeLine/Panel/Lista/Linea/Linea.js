import Etiqueta from './Etiqueta.js'
import Contenido from './Contenido/Contenido.js'

const Linea = (props) => {
  let mostrarEtiqueta = props.linea.etiqueta !== undefined
  return (
    <li>
      <div className='block'>
        {mostrarEtiqueta &&
        <Etiqueta etiqueta={props.linea.etiqueta} />
        }
        <Contenido linea={props.linea} />
      </div>
    </li>

  )
}

export default Linea
