import MarkDown from 'react-remarkable'

const Descripcion = (props) => (
  <div className='excerpt'>
    <MarkDown>{props.descripcion}</MarkDown>
  </div>

)

export default Descripcion
