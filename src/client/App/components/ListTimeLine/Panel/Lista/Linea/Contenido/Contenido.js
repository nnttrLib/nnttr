import Titulo from './Titulo.js'
import SubTitulo from './SubTitulo.js'
import Descripcion from './Descripcion.js'

const Contenido = (props) => (
  <div className='block_content'>
    <Titulo titulo={props.linea.titulo} />
    <SubTitulo subTitulo={props.linea.subTitulo} />
    <Descripcion descripcion={props.linea.descripcion} />
  </div>
)

export default Contenido
