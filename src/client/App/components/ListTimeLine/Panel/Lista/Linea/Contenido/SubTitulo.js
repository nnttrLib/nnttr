const SubTitulo = (props) => (
  <div className='byline'>
    <span>{props.subTitulo}</span>
  </div>

)

export default SubTitulo
