const Titulo = (props) => (
  <div className='x_title'>
    <h2>
      {props.title}
    </h2>
    {/* Si quitamos este span visualmente la línea de separación se superpone al título de h2 */}
    <span>&nbsp;</span>
  </div>
)

export default Titulo
