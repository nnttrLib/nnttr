import React from 'react'
import PropTypes from 'prop-types'

class Modal extends React.Component {
  render () {
    const props = this.props
    return (
      <div className='modal fade'
        id={props.id}
        tabIndex='-1'
        role='dialog'
        aria-labelledby='myModalLabel'
      >
        <div className={'modal-dialog ' + props.modalSize} role='document'>
          <div className='modal-content'>
            {(props.showHeader || props.header) &&
              <div className='modal-header'>
                <button
                  type='button'
                  className='close'
                  data-dismiss='modal'
                  aria-label='Close'
                  onClick={props.handleClickClose}>
                  <span aria-hidden='true'>&times;</span>
                </button>
                {props.header}
              </div>
            }
            <div className='modal-body'>
              {props.children}
            </div>
            {(props.showFooter || props.footer) &&
              <div className='modal-footer'>
                {props.footer}
              </div>
            }
          </div>
        </div>
      </div>

    )
  }
}

Modal.propTypes = {
  handleClickClose: PropTypes.func,
  header: PropTypes.node,
  footer: PropTypes.node,
  modalSize: PropTypes.string
}

Modal.defaultProps = {
  showHeader: true,
  showFooter: true,
  modalSize: ''
}

export default Modal
