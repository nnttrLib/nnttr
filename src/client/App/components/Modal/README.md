# Comopnente Modal

Este componente muestra una ventana modal con el contenido que se ponga como **children**

Permite especificar un header y/o un footer, o ocularlos.

## Propiedades

|nombre|valor por defecto|descripcion
|---|---|---|
|handleClickClose|null|Función que se lanza al hacer click en la esquina superior derecha para cerrar el modal|
|showHeader|true|Muestra el panel de cabecera con un botón para cerrar la ventana|
|header|null|puede ser un string o un componente|
|showFooter|true|Muestra un panel inferior|
|footer|null|puede ser un string o un componente|

## Ejemplo de uso

En el módulo donde lo utilicemos tenemos que importar la libreria

```javascript
// Importamos la libreria
import nnttr from 'nnttr'

// en el render tenemos que añadir al final el componente

/* Codigo del comopnente ... */

   render () {
     return (
       <Layaout>
         {/* Otros Comopnentes...  */}
         
         {/* En Algún componente hay que configurar para la activación del modal */}
            <a
              href="#"
               data-toggle="modal"
               data-target="#Ficha"
               data-keyboard="true"
               data-backdrop="static"
            >
              <u>
                Ver Ficha
              </u>
            </a>

         {/* Otros Componentes...  */}
                
        <Modal id="Ficha">
          {/* Aquí iría el comopnente o componentes que queremos visualizar en la ventana modal */}
        </Modal>
       </Layaout>
     )
   }


```