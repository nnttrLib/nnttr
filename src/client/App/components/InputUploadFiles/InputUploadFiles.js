import React from 'react'
import PropTypes from 'prop-types'

class InputUploadFiles extends React.Component {
  constructor (props) {
    super(props)
    this.handleChangeUploadFile = this.handleChangeUploadFile.bind(this)
    this.handleChangeUploadMultiplesFiles = this.handleChangeUploadMultiplesFiles.bind(this)
  }

  handleChangeUploadFile (event) {
    const { target: { validity, files: [file] } } = event
    validity.valid &&
    this.props.mutationSingleUpload({
      variables: { file },
      update: (proxy,  data ) => {
        // Si hemos pasado una función update la ejecutamos
        if (this.props.update) {
          this.props.update(proxy, data)
        }
      }
    })
  }

  handleChangeUploadMultiplesFiles (event) {
    const { target: { validity, files: [file] } } = event
    validity.valid &&
    this.props.mutationMultipleUpload({
      variables: { files: event.target.files },
      update: (proxy, data) => {
        // Si hemos pasado una función update la ejecutamos
        if (this.props.update) {
          this.props.update(proxy, data)
        }
      }
    })
  }

  render () {
    const props = this.props
    // En función del parámetro multiple elegimos uno u otro manejador
    let handleOnChange = props.multiple === true ? this.handleChangeUploadMultiplesFiles : this.handleChangeUploadFile
    // Si hemos pasado esta propiedad la utilizamos directamente
    if (props.handleChangeUpload) {
      handleOnChange = props.handleChangeUpload
    }

    return (
     <input
       type="file"
       className={props.className}
       required={props.required}
       multiple={props.multiple}
       onChange={handleOnChange}
     />
    )
  }
}

InputUploadFiles.propTypes = {
  handleChangeUpload: PropTypes.func,
  mutationSingleUpload: PropTypes.func,
  mutationMultipleUpload: PropTypes.func,
  update: PropTypes.func,

  multiple: PropTypes.bool,
  required: PropTypes.bool,
  className: PropTypes.string
}

InputUploadFiles.defaultProps = {
  multiple: true,
  required: true
}

export default InputUploadFiles


