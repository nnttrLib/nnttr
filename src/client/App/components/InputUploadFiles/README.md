Ejemplo de uso del componente en una escena.

```javascript
import React from 'react'
import { compose } from 'react-apollo'

import Layout from 'components/Layout'
import Page from 'components/Page'

import services from 'services'

import nnttr from 'nnttr'

const InputUploadFiles = nnttr.App.components.InputUploadFiles

class UploadFile extends React.Component {
  render () {
    return (
      <Layout>
        <Page.XPanelTitle title='Ejemplo para subir ficheros al servidor'>
          <p>Subir Fichero</p>
          <p></p>
          <InputUploadFiles
            className='btn btn-default'
            mutationSingleUpload={this.props.mutationSingleUpload}
            mutationMultipleUpload={this.props.mutationMultipleUpload}
            multiple={true}
          />
        </Page.XPanelTitle>
      </Layout>
        )
  }
}

export default compose(
  services.uploadFiles.singleUpload,
  services.uploadFiles.multipleUpload,
)(UploadFile)
```