import PropTypes from 'prop-types'

const Select = (props) => (
  <select
    name={props.name}
    title={props.title}
    value={props.selectedOption}
    onChange={props.handleOnChange}
    className={props.className}>
    {props.options.map(opt => {
      return (
        <option
          key={opt}
          value={opt}>{opt}</option>
      )
    })}
  </select>
)

Select.propTypes = {
  name: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  selectedOption: PropTypes.string,
  handleOnChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string
}

Select.defaultProps = {
  className: 'form-control'
}

export default Select
