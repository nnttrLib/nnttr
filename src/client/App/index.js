import components from './components'
import scenes from './scenes'
import services from './services'

export default {
  components,
  scenes,
  services
}
