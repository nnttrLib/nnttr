import PropTypes from 'prop-types'

const LoginForm = (props) => {
  const {onSubmit, onChange, user, captionButton} = props

  return (
    <form className='form-horizontal' onSubmit={onSubmit}>
      <div className='input-group'>
        <span className='input-group-addon'><i className='glyphicon glyphicon-user' /></span>
        <input type='text'
          autoFocus
          className='form-control'
          name='username'
          placeholder='Teclea tu nombre de Usuario'
          required
          autoComplete='off'
          onChange={onChange}
          value={user.username}
        />
      </div>
      <div className='input-group'>
        <span className='input-group-addon'><i className='glyphicon glyphicon-lock' /></span>
        <input type='password'
          className='form-control'
          name='password'
          placeholder='Contraseña'
          required
          autoComplete='off'
          onChange={onChange}
          value={user.password}
        />
      </div>
      <div className='form-group'>
        <button type='submit'
          className='btn btn-lg btn-primary'
        >
          {captionButton}
        </button>
      </div>
    </form>

  )
}

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
}

LoginForm.defaultProps = {
  captionButton: 'Iniciar Sesión'
}

export default LoginForm
