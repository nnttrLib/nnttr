import React from 'react'
import moment from 'moment'
import LoginForm from './components/LoginForm'

moment.locale('es') // Ajustamos para que el formato salga en español

class Login extends React.Component {
  constructor (props) {
    super(props)
    this.processForm = this.processForm.bind(this)
    this.changeUser = this.changeUser.bind(this)
    this.isAuthenticated = this.isAuthenticated.bind(this)

    // set the initial component state
    this.state = {
      errors: {},
      user: {
        username: '',
        password: ''
      }
    }
  }

  processForm (event) {
    // prevent default action. in this case, action is the form submission event
    event.preventDefault()
    const {url} = this.props
    let urlQuery
    if (url && url.query) {
      urlQuery = url.query
      if (urlQuery.pathNamePostLogin === undefined) {
        urlQuery.pathNamePostLogin = '/'
      }
    }

    let queryArray = Object.keys(urlQuery).filter(item => item !== 'pathNamePostLogin')
    let query = ''
    if (queryArray.length > 0) {
      query = queryArray.map(item => item + '=' + url.query[item]).join('&')
      query = '?' + query
    }
    let pathNamePostLogin = urlQuery.pathNamePostLogin + query
    this.props.actions.session.callLogin(this.state.user.username, this.state.user.password, pathNamePostLogin)
  }

  changeUser (event) {
    const field = event.target.name
    const user = this.state.user
    user[field] = event.target.value

    this.setState({
      user
    })
  }

  isAuthenticated () {
    return this.props.session && this.props.session.isAuthenticated
  }

  render () {
    const {name, logoApp, titleLogo, titleName, version, fechaPublicacion, infoComponent} = this.props
    const versionFecha = 'Versión: ' + version + ' Publicada ' + moment(fechaPublicacion).fromNow() + ' el ' + moment(fechaPublicacion).format('DD/MM/YYYY HH:mm')
    const logo = logoApp || '/static/logoApp.jpg'
    return (
      <div className='login'>
        <div className='animate form login_form'>
          <p />
          <div className='col-md-3 col-sm-1 col-xs-12' />
          <div className='col-md-6 col-sm-6 col-xs-12'>
            <img src={logo} className='img-responsive center-block' width='75' title={titleLogo || versionFecha} />
            <h4 className='center-block text-center' title={titleName}>{name}</h4>
            <LoginForm
              onSubmit={this.processForm}
              onChange={this.changeUser}
              errors={this.state.errors}
              user={this.state.user}
            />
            {infoComponent}
          </div>
        </div>
      </div>
    )
  }
}

Login.displayName = 'Login'

export default Login
