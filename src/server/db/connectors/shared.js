function keyValue (field) {
  if (typeof field === 'string') {
    return '\'' + field + '\''
  } else {
    return field
  }
}

export function sqlSelect (input, simboloVariableSubstitucion) {
  if (typeof input !== 'object') throw new Error('El parametro input debe ser un objeto')
  if (input.from === undefined) throw new Error('Es obligatorio pasar un campo from en el parametro input')

  // Preparamos los campos de la Select
  // Si no pasamos un campo fields, asumimos que sacamos todos los de la tabla
  let fields = input.fields || ' * '
  // Si input.fields es un array, lo convertimos a un string de campos separados por comas.
  if (Array.isArray(fields)) fields = fields.join(', ')

  // Preparamos la clausula from
  let from = input.from
  // Si input.from es un array, lo convertimos a un string de elementos separados por comas
  if (Array.isArray(from)) from = from.join(', ')

  // Preparamos la clausula where
  let where = ''
  if (input.filter) {  // Si hemos pasado un filtro tenemos que añadirlo a la cláusula where, si es que hay.
    where = input.where === undefined ? 'where ' + input.filter : 'where (' + input.filter + ')\n        and (' + input.where + ') '
  } else {  // Si no hay definido ningún filtro
    where = input.where === undefined ? '' : 'where ' + input.where
  }

  // Preparamos la clausula groupBy
  let groupBy = input.groupBy === undefined ? '' : 'group by ' + input.groupBy

  // Preparamos la clausula having
  let having = input.having === undefined ? '' : 'having ' + input.having

  // Preparamos la clausula orderBy
  let orderBy = input.orderBy === undefined ? '' : 'order by ' + input.orderBy

  // Construimos la sentencia select
  const sql = `
                select ` + fields + `
                from ` + from + `
                ` + where + `
                ` + groupBy + `
                ` + having + `
                ` + orderBy + `
                
  `
  return {
    sql: sql,
    params: undefined
  }
}

// Para Oracle no se utiliza esta función, se ha personalizado una dentro de oracle.js
export function sqlUpdate (tabla, set, where, simboloVariableSubstitucion, returningClause = '') {
  if (typeof tabla !== 'string') throw new Error('el parámetro tabla debe ser un string')
  if (typeof set !== 'object' && typeof set !== 'string') throw new Error('El parámetro set debe ser un objeto o un string')
  if (typeof where !== 'object' && typeof where !== 'string') throw new Error('El parámetro where debe ser un objeto con tantos campos como campos tenga la clave primaria, o bien un string con la claúsula where a mano')
  if (simboloVariableSubstitucion === undefined) throw new Error('Es obligatorio pasar el simbolo con el que se tagearan las variables a sustituir por los valores en la insert')
  if (typeof returningClause !== 'string') throw new Error('El parametro returningClause tiene que venir en formato string')

  let i = 0
  let valores
  let setClause = set
  if (typeof set === 'object') {
    valores = Object.values(set)
    setClause = Object.keys(set).map(item => '\n' + item + ' = ' + simboloVariableSubstitucion + (++i)).join(', ')
  }
  let whereClause = where

  if (typeof where === 'object') {
    whereClause = Object.keys(where).map(item => item + ' = ' + keyValue(where[item])).join(' and ')
  }

  const sql = `
                update ` + tabla + `
                set ` + setClause + `
                where ` + whereClause + `
                ` + returningClause + `
  `
  return {
    sql: sql,
    params: valores
  }
}

// Para Oracle no se utiliza esta función, se ha personalizado una dentro de oracle.js
export function sqlDelete (tabla, where, simboloVariableSubstitucion, returningClause = '') {
  if (typeof tabla !== 'string') throw new Error('el parámetro tabla debe ser un string')
  if (typeof where !== 'object' && typeof where !== 'string') throw new Error('El parámetro where debe ser un objeto con tantos campos como campos tenga la clave primaria, o bien un string con la claúsula where a mano')
  if (simboloVariableSubstitucion === undefined) throw new Error('Es obligatorio pasar el simbolo con el que se tagearan las variables a sustituir por los valores en la insert')
  if (typeof returningClause !== 'string') throw new Error('El parametro returningClause tiene que venir en formato string')

  let valores
  let whereClause = where

  if (typeof where === 'object') {
    whereClause = Object.keys(where).map(item => item + ' = ' + keyValue(where[item])).join(' and ')
    valores = Object.values(where)
  }

  const sql = `
                delete from ` + tabla + `
                where ` + whereClause + `
                ` + returningClause + `
  `
  return {
    sql: sql,
    params: valores
  }
}

// Para Oracle no se utiliza esta función, se ha personalizado una dentro de oracle.js
export function sqlInsert (tabla, input, simboloVariableSubstitucion, returningClause = '') {
  if (typeof tabla !== 'string') throw new Error('el parámetro tabla debe ser un string')
  if (typeof input !== 'object') throw new Error('El parámetro input debe ser un objeto')
  if (simboloVariableSubstitucion === undefined) throw new Error('Es obligatorio pasar el simbolo con el que se tagearan las variables a sustituir por los valores en la insert')
  if (typeof returningClause !== 'string') throw new Error('El parametro returningClause tiene que venir en formato string')

  // Extraemos el nombre de los campos del objeto input
  const campos = '(' + Object.keys(input).join(', ') + ')'
  let i = 0
  // Creamos tantas variables como campos vamos a insertar
  const variables = '(' + Object.keys(input).map(item => simboloVariableSubstitucion + (++i)).join(', ') + ')'

  // Los valores que se van a insertar los sacamos de input
  const valores = Object.values(input)

  // Creamos la sentencia sql
  const sql = `
                 insert into ` + tabla + `
                 ` + campos + ` 
                 values
                 ` + variables + `
                 ` + returningClause + `
              `
  // Deveolvemos un objeto con dos campos, uno con la sql y otra con los
  // valores que se van a insertar, para que luego se lo podamos pasar a la conexión
  // correspondiente.
  return {
    sql: sql,
    params: valores
  }
}
