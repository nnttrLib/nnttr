// ********************************************
// Driver de acceso a base de datos PostgreSql
// ********************************************
import * as shared from './shared'

const SIMBOLO_VARIABLE_SUSTITUCION = '$'

class postgreSql {
  constructor (pgp, connection) {
    this._db = pgp(connection)
    this._simboloVariableSubstitucion = SIMBOLO_VARIABLE_SUSTITUCION
  }

  get db () {
    return this._db
  }

  selectOne (sql, params) {
    return this._db.one(sql, params)
      .then((result) => result)
      .catch((error) => {
        throw new Error(error)
      })
  }

  select (sql, params) {
    return this._db.any(sql, params)
      .then((result) => result)
      .catch((error) => {
        throw new Error(error)
      })
  }

  insert (sql, params) {
    return this._db.one(sql, params)
      .then((result) => result)
      .catch((error) => {
        throw new Error(error)
      })
  }

  update (sql, params) {
    return this._db.one(sql, params)
      .then((result) => result)
      .catch((error) => {
        throw new Error(error)
      })
  }

  delete (sql, params) {
    return this._db.result(sql, params)
      .then((result) => {
        // Preparamos las cosas para si se ha puesto returning * devolvamos el valor del registro borrado
        if (result.rows && Array.isArray(result.rows) && result.rows.length > 0) {
          return result.rows[0]
        } else {
          return result
        }
      })
      .catch((error) => {
        throw new Error(error)
      })
  }

  returningClause (fieldsReturning) {
    let result = fieldsReturning
    if (fieldsReturning && Array.isArray(fieldsReturning)) {
      result = fieldsReturning.join(', ')
    }

    if (result !== undefined) {
      result = ' returning ' + result + ' '
    }

    return result
  }

  sqlUpdate (tabla, set, where, fieldsReturning) {
    return new Promise((resolve, reject) => {
      // Si no pasamos nada en fieldsReturning, asumimos que devolvemos todos los campos de la insert
      const camposReturning = fieldsReturning || ' * '
      // Construimos la sentencia sql para la insert
      const sqlUpdate = shared.sqlUpdate(tabla, set, where, this._simboloVariableSubstitucion, this.returningClause(camposReturning))

      return resolve(sqlUpdate)
    })
  }

  sqlInsert (tabla, input, fieldsReturning) {
    return new Promise((resolve, reject) => {
      // Si no pasamos nada en fieldsReturning, asumimos que devolvemos todos los campos de la insert
      const camposReturning = fieldsReturning || ' * '
      // Construimos la sentencia sql para la insert
      const sqlInsert = shared.sqlInsert(tabla, input, this._simboloVariableSubstitucion, this.returningClause(camposReturning))

      return resolve(sqlInsert)
    })
  }

  sqlDelete (tabla, where, fieldsReturning) {
    return new Promise((resolve, reject) => {
      // Si no pasamos nada en fieldsReturning, asumimos que devolvemos todos los campos de la insert
      const camposReturning = fieldsReturning || ' returning * '
      // Construimos la sentencia sql para la insert
      const sqlUpdate = shared.sqlDelete(tabla, where, this._simboloVariableSubstitucion, camposReturning)

      return resolve(sqlUpdate)
    })
  }

  sqlSelect (input) {
    return new Promise((resolve, reject) => {
      return resolve(shared.sqlSelect(input, this._simboloVariableSubstitucion))
    })
  }
}

export default postgreSql
