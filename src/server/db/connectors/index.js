import PostgreSql from './postgreSql'
import Oracle from './oracle'

export default {
  Oracle,
  PostgreSql
}
