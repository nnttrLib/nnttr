// ********************************************
// Driver de acceso a base de datos Oracle
// ********************************************

import moment from 'moment'

import * as shared from './shared'

moment.locale('es')

const SIMBOLO_VARIABLE_SUSTITUCION = ':'
const PREFIJO = 'RESULTADO_'

class Oracle {
  constructor (oracledb, connection, defaultOptions) {
    this._simboloVariableSubstitucion = SIMBOLO_VARIABLE_SUSTITUCION
    this._config = connection
    this._oracledb = oracledb
    if (defaultOptions) {
      this._defaultOptions = defaultOptions
    } else {
      this._defaultOptions = {
        outFormat: this._oracledb.OBJECT,
        autoCommit: true,
        extendedMetaData: true,
        maxRows: 10000
      }
    }

    this._inicio = new Date()

    setTimeout(() => {
      console.log('Creando el Pool de conexión a Oracle...')
    }, 1000)

    oracledb.createPool(this._config)
      .then((pool, error) => {
        if (error) {
          console.log('ERROR: ', new Date(), ': createPool() callback: ')
          console.log(error)
          return
        }
        this._pool = pool
        let diff = new Date() - this._inicio
        console.log('pool de conexión a Oracle Creado en ' + diff + ' ms')
      })
  }

  isDate (val) {
    let formats = [
      moment.ISO_8601
    ]
    let esNumero = Number(val)

//    return isNaN(esNumero) && !isNaN(Date.parse(val))
    return isNaN(esNumero) && moment(val, formats, true).isValid()
  }

  doRelease (connection) {
    connection.release(
      function (err) {
        if (err) {
          console.error(err.message)
        }
      })
  }

  sql (sql, params = {}) {
    return new Promise((resolve, reject) => {
//      this._oracledb.getConnection(this._config)
      this._pool.getConnection()
        .then(conn => {
          conn.execute(sql, params, this._defaultOptions)
            .then(result => {
              this.doRelease(conn)
              return resolve(result.rows)
            })
            .catch(error => {
              this.doRelease(conn)
              return reject(error)
            })
        })
        .catch(error => {
          return reject(error)
        })
    })
  }

  sqlReturning (sql, params = {}) {
    return new Promise((resolve, reject) => {
//      this._oracledb.getConnection(this._config)
      this._pool.getConnection()
        .then(conn => {
          conn.execute(sql, params, this._defaultOptions)
            .then(result => {
              this.doRelease(conn)
              // Preparamos el objeto con los datos de retorno
              let outBinds = {}
              Object.keys(result.outBinds).map(item => {
                let campo = item.substring(PREFIJO.length, 1000)
                outBinds[campo] = result.outBinds[item][0]
              })
              return resolve(outBinds)
            })
            .catch(error => {
              this.doRelease(conn)
              return reject(error)
            })
        })
        .catch(error => {
          return reject(error)
        })
    })
  }

  selectOne (sql, params) {
    return this.sql(sql, params)
      .then((result) => {
        let payload = {}

        if (result && Array.isArray(result) && result.length > 0) {
          payload = result[0]
        } else {
          payload = result
        }
        return payload
      })
      .catch((error) => {
        throw new Error(error)
      })
  }

  select (sql, params) {
    return this.sql(sql, params)
      .then((result) => result)
      .catch((error) => {
        throw new Error(error)
      })
  }

  insert (sql, params) {
    return this.sqlReturning(sql, params)
      .then((result) => result)
      .catch((error) => {
        throw new Error(error)
      })
  }

  update (sql, params) {
    return this.sqlReturning(sql, params)
      .then((result) => result)
      .catch((error) => {
        throw new Error(error)
      })
  }

  delete (sql, params) {
    return this.sqlReturning(sql, params)
      .then((result) => result)
      .catch((error) => {
        throw new Error(error)
      })
  }

  sqlUpdate (tabla, set, where, returningClause = true) {
    return new Promise((resolve, reject) => {
      if (typeof tabla !== 'string') throw new Error('el parámetro tabla debe ser un string')
      if (typeof set !== 'object' && typeof set !== 'string') throw new Error('El parámetro set debe ser un objeto o un string')
      if (typeof where !== 'object' && typeof where !== 'string') throw new Error('El parámetro where debe ser un objeto con tantos campos como campos tenga la clave primaria, o bien un string con la claúsula where a mano')
      if (returningClause !== undefined && typeof returningClause !== 'boolean') throw new Error('El parametro returningClause tiene que ser boolean')

      let params = {}

      const whereClause = Object.keys(where).map(item => {
        if (typeof where[item] === 'string' && this.isDate(where[item])) {
          params['WHERE_' + item.toUpperCase()] = new Date(where[item])
        } else {
          params['WHERE_' + item.toUpperCase()] = where[item]
        }
        return item + ' = ' + this._simboloVariableSubstitucion + 'WHERE_' + item.toUpperCase()
      }).join(' and ')

      // Extraemos el nombre de los campos del objeto set
      const setClause = Object.keys(set).map(item => '\n' + item.toUpperCase() + ' = ' + this._simboloVariableSubstitucion + item.toUpperCase()).join(', ')
      // Los valores que se van a insertar los sacamos de set

      Object.keys(set).map(item => {
        if (typeof set[item] === 'string' && this.isDate(set[item])) {
          params[item.toUpperCase()] = new Date(set[item])
        } else {
          params[item.toUpperCase()] = set[item]
        }
      })

      let sql = `
                update ` + tabla + `
                set ` + setClause + `
                where ` + whereClause + `
  `
      let result = {
        sql: sql,
        params: params
      }

      if (returningClause) {  // Si queremos que la sentencia devuelva los campos
        // Obtenemos primero los metadatos de la tabla para poder construir luego la sentencia returning
        return this.leerMetaDatos(tabla)
          .then(metaData => {
            let returnClause = this.returningClause(metaData)
            // Construimos la sentencia sql para la insert
            sql = sql + returnClause.sql

            Object.keys(returnClause.params).map(item => {
              params[item] = returnClause.params[item]
            })

            result = {
              sql,
              params
            }

            return resolve(result)
          })
          .catch(error => {
            return reject(error)
          })
      } else {
        return resolve(result)
      }
    })
  }

  leerMetaDatos (tabla) {
    if (typeof tabla !== 'string') throw new Error('El parametro tabla debe ser un string')
    if (!tabla.includes('.')) throw new Error('el contenigo del parámetro tabla debe venir en el formato: "esquema.tabla" ')
    let owner = tabla.split('.')[0].toUpperCase()
    let tableName = tabla.split('.')[1].toUpperCase()
    let sqlMetadata = "select COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DATA_PRECISION, NULLABLE from all_tab_columns where data_type<>'BLOB' and owner='" + owner + '\' and table_name=\'' + tableName + '\' '
    return this.sql(sqlMetadata)
      .then(result => {
        return Promise.resolve(result)
      })
      .catch(error => {
        return Promise.reject(error)
      })
  }

  returningClause (metaData) {
    const FIELDS = metaData.map(item => item.COLUMN_NAME).join(', ')
    const VARIABLES = metaData.map(item => this._simboloVariableSubstitucion + PREFIJO + item.COLUMN_NAME).join(', ')

    const sqlClause = 'RETURNING ' + FIELDS + ' INTO ' + VARIABLES
    let params = {}
    let tipo
    metaData.map(item => {
      switch (item.DATA_TYPE) {
        case 'VARCHAR2':
          tipo = this._oracledb.STRING
          break
        case 'DATE':
          tipo = this._oracledb.DATE
          break
        case 'NUMBER':
          tipo = this._oracledb.NUMBER
          break
        default:
          tipo = this._oracledb.STRING
      }

      params[PREFIJO + item.COLUMN_NAME] = {
        type: tipo,
        dir: this._oracledb.BIND_OUT
      }
    })

    return {
      sql: sqlClause,
      params
    }
  }

  sqlInsert (tabla, input, returningClause = true) {
    return new Promise((resolve, reject) => {
      if (typeof tabla !== 'string') throw new Error('el parámetro tabla debe ser un string')
      if (typeof input !== 'object') throw new Error('El parámetro input debe ser un objeto')
      if (returningClause !== undefined && typeof returningClause !== 'boolean') throw new Error('El parametro returningClause tiene que ser boolean')

      // Extraemos el nombre de los campos del objeto input
      const campos = '(' + Object.keys(input).join(', ') + ')'
      // Creamos tantas variables como campos vamos a insertar
      const variables = '(' + Object.keys(input).map(item => this._simboloVariableSubstitucion + item.toUpperCase()).join(', ') + ')'
      // Los valores que se van a insertar los sacamos de input
      let params = {}

      Object.keys(input).map(item => {
        if (typeof input[item] === 'string' && this.isDate(input[item])) {
          params[item.toUpperCase()] = new Date(input[item])
        } else {
          params[item.toUpperCase()] = input[item]
        }
      })

      // Creamos la sentencia sql
      let sql = `
                 insert into ` + tabla + `
                 ` + campos + ` 
                 values
                 ` + variables + `
              `

      let result = {
        sql,
        params
      }

      if (returningClause) {  // Si queremos que la sentencia devuelva los campos
        // Obtenemos primero los metadatos de la tabla para poder construir luego la sentencia returning
        return this.leerMetaDatos(tabla)
          .then(metaData => {
            let returnClause = this.returningClause(metaData)
            // Construimos la sentencia sql para la insert
            sql = sql + returnClause.sql

            Object.keys(returnClause.params).map(item => {
              params[item] = returnClause.params[item]
            })

            result = {
              sql,
              params
            }

            return resolve(result)
          })
          .catch(error => {
            return reject(error)
          })
      } else {
        return resolve(result)
      }
    })
  }

  sqlDelete (tabla, where, returningClause = true) {
    return new Promise((resolve, reject) => {
      if (typeof tabla !== 'string') throw new Error('el parámetro tabla debe ser un string')
      if (typeof where !== 'object' && typeof where !== 'string') throw new Error('El parámetro where debe ser un objeto con tantos campos como campos tenga la clave primaria, o bien un string con la claúsula where a mano')
      if (returningClause !== undefined && typeof returningClause !== 'boolean') throw new Error('El parametro returningClause tiene que ser boolean')

      let params = {}

      const whereClause = Object.keys(where).map(item => {
        if (typeof where[item] === 'string' && this.isDate(where[item])) {
          params['WHERE_' + item.toUpperCase()] = new Date(where[item])
        } else {
          params['WHERE_' + item.toUpperCase()] = where[item]
        }
        return item + ' = ' + this._simboloVariableSubstitucion + 'WHERE_' + item.toUpperCase()
      }).join(' and ')

      let sql = `
                delete from ` + tabla + `
                where ` + whereClause + `
       `
      let result = {
        sql: sql,
        params: params
      }

      if (returningClause) {  // Si queremos que la sentencia devuelva los campos
        // Obtenemos primero los metadatos de la tabla para poder construir luego la sentencia returning
        return this.leerMetaDatos(tabla)
          .then(metaData => {
            let returnClause = this.returningClause(metaData)
            // Construimos la sentencia sql para la insert
            sql = sql + returnClause.sql

            Object.keys(returnClause.params).map(item => {
              params[item] = returnClause.params[item]
            })

            result = {
              sql,
              params
            }

            return resolve(result)
          })
          .catch(error => {
            return reject(error)
          })
      } else {
        return resolve(result)
      }
    })
  }

  sqlSelect (input) {
    return new Promise((resolve, reject) => {
      return resolve(shared.sqlSelect(input, this._simboloVariableSubstitucion))
    })
  }

  sqlSelectPaginate (input, connection) {
    return new Promise((resolve, reject) => {
      if (input.orderBy === undefined) throw new Error('Es obligatorio pasar un campo order By en el parametro input')
      if (input.fieldCursor === undefined) throw new Error('Es obligatorio pasar un campo fieldCursor en el parametro input, que se usuará para el cursor')

      let inputTotalCount = Object.assign({}, input)
      let inputHasNextPage = Object.assign({}, input)
      let inputHasPreviousPage = Object.assign({}, input)
      // Preparamos una subselect para calcular el nº total de registros
      inputTotalCount.fields = 'count(*)'
      delete inputTotalCount.orderBy
      let sqlTotalCount = shared.sqlSelect(inputTotalCount, this._simboloVariableSubstitucion)

      // Preparamos una subselect para calcular si hay una página adicional
      inputHasNextPage.fields = ' case when count(*)=0 then 0 else 1 end  '
      delete inputHasNextPage.orderBy

      // Preparamos una subselect para calcular si hay una página anterior
      inputHasPreviousPage.fields = ' case when count(*)=0 then 0 else 1 end  '
      delete inputHasPreviousPage.orderBy

      let fields = input.fields || ' a.*, ' + input.fieldCursor + ' as GRAPHQL_CURSOR '
      fields = fields + ', row_number() over (order by ' + input.orderBy + ') rnk '
      fields = fields + ', (' + sqlTotalCount.sql + ') GRAPHQL_TOTAL_COUNT '
      input.fields = fields
      input.from = input.from + ' a'

      let afterCursor = connection.after || ''

      // si la ordenación es descendente la comparación es al reves
      let operadorNext = '>' // Por defecto pensamos que la ordenación es ascendente
      if (input.orderDesc) {
        operadorNext = '<'
      }

      if (afterCursor !== '') {
        // Decodificamos el cursor que nos va a venir en formato base64
        afterCursor = Buffer.from(afterCursor, 'base64').toString()

        let whereInputAfterCursor = input.fieldCursor + operadorNext + "'" + afterCursor + "'"
        let whereInputHasNextPageAfterCursor = input.fieldCursor + operadorNext + ' GRAPHQL_CURSOR '

        input.where = input.where ? input.where + ' and (' + whereInputAfterCursor + ') ' : whereInputAfterCursor
        inputHasNextPage.where = inputHasNextPage.where ? inputHasNextPage.where + ' and (' + whereInputHasNextPageAfterCursor + ') ' : whereInputHasNextPageAfterCursor
      }

      let sqlHasNextPage = shared.sqlSelect(inputHasNextPage, this._simboloVariableSubstitucion)
      let sql = shared.sqlSelect(input, this._simboloVariableSubstitucion)
      sql.sql = `
        select b.*, (` + sqlHasNextPage.sql + `) as GRAPHQL_HAS_NEXT_PAGE 
        from (
           ` + sql.sql + `
          ) b
        where rnk <= ` + connection.first + `  
      `
      return resolve(sql)
    })
  }
}

export default Oracle
