import connectors from './connectors/index'
import models from './models/index'

export default {
  connectors,
  models
}
