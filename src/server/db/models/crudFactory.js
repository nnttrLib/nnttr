// Función a la que le pasamos el driver de una base de datos y el nombre de una tabla
// y nos devuelve un objeto donde cada campo es una de las funciones CRUD que luego se utilizaran.
export default function crud (db, tabla) {
  return {
    select: (input = {}) => {
      input.from = tabla
      return db.sqlSelect(input)
        .then(result => db.select(result.sql, result.params))
    },
    selectOne: (input = {}) => {
      input.from = tabla
      return db.sqlSelect(input)
        .then(result => db.selectOne(result.sql, result.params))
    },
    create: (input) => {
      return db.sqlInsert(tabla, input)
        .then(result => db.insert(result.sql, result.params))
    },
    update: (set, where) => {
      return db.sqlUpdate(tabla, set, where)
        .then(result => db.update(result.sql, result.params))
    },
    remove: (where) => {
      return db.sqlDelete(tabla, where)
        .then(result => db.delete(result.sql, result.params))
    }
  }
}
