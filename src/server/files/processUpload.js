import fse from 'fs-extra'
import path from 'path'

const storeFS = (params) => {
  const stream = params.stream
  const filename = params.filename
  return new Promise((resolve, reject) =>
    stream
      .on('error', error => {
        if (stream.truncated)
        // Delete the truncated file
          fse.unlinkSync(path)
        reject(error)
      })
      .pipe(fse.createWriteStream(filename))
      .on('error', error => reject(error))
      .on('finish', () => resolve({ filename }))
  )
}

const processUpload = async (ruta, nuevoNombreFichero, upload) => {
  // Obtenemos el nombre del fichero que hemos subido en filename
  const { stream, filename, mimetype, encoding } = await upload
  // Obtenemos la extensión del fichero
  const extension = path.extname(filename).toLowerCase()
  // Construimos el nombre del fichero a grabar
  const fichero = ruta + '/' + nuevoNombreFichero + extension

  // Grabamos el fichero con la ruta y extensión adecuada
  await storeFS({ stream, filename: fichero })

  // Devolvemos el nombre del fichero que incluye toda la ruta,
  // y la extensión del fichero.
  return { fichero, extension, filename, mimetype, encoding }
}

export default processUpload
