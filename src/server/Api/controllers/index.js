import login from './login'
import passport from './passport'
import notFound from './notFound'

export default {
  login,
  passport,
  notFound
}
