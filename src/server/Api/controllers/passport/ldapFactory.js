import LdapStrategy from 'passport-ldapauth'

const SEARCH_FILTER = '(&(objectcategory=person)(objectclass=user)(|(samaccountname={{username}})(mail={{username}})))'
const SEARCH_ATTRIBUTES = ['displayName', 'mail']

function getLdapConfigFactory (ldapUrl, ldapSufijoUsuario, ldapSearchBase, searchFilter, searchAttributes) {
  return (req, callback) => {
    process.nextTick(function () {
      let userName = req.query.username || req.body.username
      let password = req.query.password || req.body.password
      let opts = {
        server: {
          url: ldapUrl, // 'ldap://ldap.begar.es',
          bindDn: userName + ldapSufijoUsuario, // '@ad.begar.es',
          bindCredentials: password,
          searchBase: ldapSearchBase, // 'OU=USUARIOSGRUPO,OU=Usuarios,DC=ad,DC=begar,DC=es',
          searchFilter: searchFilter,
          searchAttributes: searchAttributes
        }
      }
      callback(null, opts)
    })
  }
}

function ldapFactory (ldapUrl, ldapSufijoUsuario, ldapSearchBase, searchFilter = SEARCH_FILTER, searchAttributes = SEARCH_ATTRIBUTES) {
  return (passport) => {
    const getLdapConfig = getLdapConfigFactory(ldapUrl, ldapSufijoUsuario, ldapSearchBase, searchFilter, searchAttributes)

    passport.use(new LdapStrategy(getLdapConfig, (user, done) => {
      return done(null, user)
    }))
  }
}

export default ldapFactory
