import jwt from 'jsonwebtoken'

function usuarioEnantar (oracle, usuario) {
  const sqlUsuario = ' select u.*,\n' +
    '      (select e.nombre from libra.empresas_conta e where e.codigo=u.EMPRESA_ACTUAL ) as nombre_empresa,\n' +
    '      (select l.nombre_localizacion from serviciossoporte.localizacion l where l.id_localizacion=u.id_localizacion) as nombre_localizacion, \n' +
    '      (\n' +
    '       select t.codigo_trabajador\n' +
    '       from libra.rh_trabajadores t\n' +
    '       where t.empresa=u.EMPRESA_ACTUAL and t.dni=u.nif\n' +
    '             and libra.rh_pkfechas.situacion(t.empresa, t.subempresa, t.codigo_trabajador, sysdate)=\'A\'\n' +
    '       ) as codigo_trabajador ,' +

    '      (\n' +
    '       select t.subempresa\n' +
    '       from libra.rh_trabajadores t\n' +
    '       where t.empresa=u.EMPRESA_ACTUAL and t.dni=u.nif\n' +
    '             and libra.rh_pkfechas.situacion(t.empresa, t.subempresa, t.codigo_trabajador, sysdate)=\'A\'\n' +
    '       ) as subempresa ' +

    ' from begar.ver_usuarios_grupo u\n' +
    ' where u.login=:login'

  let res

  return oracle.select(sqlUsuario, {
    login: usuario.userName.toUpperCase()
  })
    .then((resultado) => {
      if (resultado.length === 0) {
        throw new Error('Usurio no existe')
      } else if (resultado.length > 1) {
        throw new Error('Hay más de dos usuarios con el mismo username')
      } else if (resultado.length === 1) {
        // Si solo devuelve un registro, construimos el objeto usuario
        // con las propiedades que nos interese y lo devolvemos.
        usuario.password = ''
        usuario.id_usuario = resultado[0].ID_USUARIO
        usuario.nombre = resultado[0].NOMBRE_USUARIO
        usuario.apellidos = resultado[0].APELLIDO_USUARIO
        usuario.email = resultado[0].CORREO_ELECTRONICO.toLowerCase()
        usuario.id_empresa = resultado[0].EMPRESA_ACTUAL
        usuario.subempresa = resultado[0].SUBEMPRESA
        usuario.id_localizacion = resultado[0].ID_LOCALIZACION
        usuario.nombre_empresa = resultado[0].NOMBRE_EMPRESA
        usuario.nombre_localizacion = resultado[0].NOMBRE_LOCALIZACION
        usuario.nif = resultado[0].NIF
        // si el campo codigo_trabajador está vacío quiere decir que este usuario
        // no está dado de alta en nóminas en la empresa que tiene asiganda como empresa actual
        usuario.codigo_trabajador = resultado[0].CODIGO_TRABAJADOR

        res = usuario
      }
      return res
    })
    .catch((error) => {
      throw new Error('Error en la consulta oracle: ' + JSON.stringify(error))
    })
}

// Proceso que se encarga de crear el usuario en la tabla postgress, si no existe, y de
// actualizar los datos del usuario si existe.
function actualizaUsers (postgresql, usuario, ip) {
  // Primero comprobamos si el usuario ya existe en la tabla users
  return postgresql.select('SELECT * FROM users where id = $1', usuario.id_usuario)
    .then((resultado) => {
      if (resultado.length === 1) { // Si lo ha encontrado, actualizamos la información importante:
        return postgresql.update('UPDATE users ' +
          'set sign_in_count = sign_in_count + 1, ' +
          'last_sign_in_at   = current_sign_in_at, ' +
          'current_sign_in_ip = $1,' +
          'current_sign_in_at= now() ' +
          'where id=$2 returning * ', [ip, usuario.id_usuario])
          .then(userPg => {
            return resultado[0]
          })
          .catch((error) => {
            throw new Error('Error al actualizar el usuario: ' + error)
          })
      } else { // Si no lo encuentra creamos el usuario
        return postgresql.insert('INSERT into users ' +
          '(id, email, name, surname, sign_in_count, current_sign_in_at, current_sign_in_ip) ' +
          'values' +
          '($1, $2, $3, $4, 1, now(), $5 ) returning * ', [usuario.id_usuario, usuario.email, usuario.nombre, usuario.apellidos, ip], 'id')

          .catch((error) => {
            throw new Error('Error al crear el usuario: ' + error)
          })
      }
    })
    .catch(error => {
      throw new Error('Error al buscar usuarios en postgresql\n' + JSON.stringify(error))
    })
}

function accesoAplicacion (postgresql, usuario) {
  const sqlPermisosAplicacion = `
                 select count(*) 
                 from v_users_apps ua
                  where (ua.id_user=$1 and ua.id_app=$2) or (ua.id_app=$2 and ua.acceso_universal)
                `
  // Empresas permitidas para el usuario que ha hecho login y para la aplicacion
  const sqlEmpresasPermitidas = `
                        select distinct ea.id_empresa, ea.nombre_empresa
                        from v_empresas_apps ea,
                             v_empresas_users eu
                        where ea.id_empresa=eu.id_empresa
                              and eu.id_user=$1
                              and ea.id_app=$2
                                  `

  let paramsPermisosAplicacion = [usuario.id_usuario, usuario.id_app]
  let paramsEmpresasPermitidas = [usuario.id_usuario, usuario.id_app]

  let res = {
    tienePermiso: false,
    empresasPermitidas: []
  }

  return postgresql.select(sqlPermisosAplicacion, paramsPermisosAplicacion)
    .then((resultado) => {
      if (resultado[0].count === '0') {
        res.tienePermiso = false
        return res
      } else {
        res.tienePermiso = true
        return postgresql.select(sqlEmpresasPermitidas, paramsEmpresasPermitidas)
          .then((resultado) => {
            res.empresasPermitidas = resultado
            return res
          })
          .catch((error) => {
            throw new Error(error)
          })
      }
    })
    .catch((error) => {
      throw new Error('Error al consultar permisos a la aplicación: ' + JSON.stringify(error))
    })
}

function loginUserEnantarFactory (oracle, postgresql, secret = 'MiClaveSuperSecreta', expiresIn = 60 * 60 * 24 * 30) {
  return (req, res, next) => {
    let userName = req.query.username || req.body.username
    let password = req.query.password || req.body.password
    let idApp = req.query.id_app || req.body.id_app
    let usuario = {
      userName: userName,
      password: password,
      id_app: idApp
    }
    if (!idApp) {
      res.status(401).json({
        success: false,
        message: 'Error al hacer Login, Aplicación no autorizada '
      })
    } else {
      try {
        return usuarioEnantar(oracle, usuario)
          .then(usuario => {
            if (!usuario) {
              res.status(401).json({
                success: false,
                message: 'Error al hacer Login.'
              })
            } else {
              return accesoAplicacion(postgresql, usuario)
                .then((resultadoAccesoAplicacion) => {
                  if (!resultadoAccesoAplicacion.tienePermiso) {
                    res.status(401).json({
                      success: false,
                      permisosApp: false,
                      message: 'Error al hacer Login, usuario no tiene acceso a esta aplicación '
                    })
                    return
                  }
                  try {
                    let ipTmp = req.headers['x-forwarded-for'] || req.connection.remoteAddress
                    let ip = ipTmp
                    if (ipTmp.substring(0, 7) === '::ffff:') {
                      ip = ipTmp.substring(7, 100)
                    }

                    return actualizaUsers(postgresql, usuario, ip)
                      .then((respuesta) => {
                        const payload = {
                          tipo: 'AUTH_TOKEN_API_GENERAL',
                          id_app: Number(idApp),
                          usuario: usuario.userName,
                          ip: ip,
                          id_usuario: usuario.id_usuario,
                          email: usuario.email,
                          id_empresa: usuario.id_empresa,
                          subempresa: usuario.subempresa,
                          id_localizacion: usuario.id_localizacion,
                          nombre: usuario.nombre,
                          apellidos: usuario.apellidos,
                          nif: usuario.nif,
                          codigo_trabajador: usuario.codigo_trabajador
                        }
                        let token = jwt.sign(payload, secret, {
                          expiresIn: expiresIn // expires in 30 dias. Se expresa en segundos.
                        })
                        const payloadFicheros = {
                          tipo: 'AUTH_TOKEN_API_GENERAL',
                          id_app: Number(idApp),
                          id_usuario: usuario.id_usuario
                        }

                        let tokenFicheros = jwt.sign(payloadFicheros, secret, {
                          expiresIn: expiresIn // expires in 30 dias. Se expresa en segundos.
                        })

                        usuario.admin = respuesta.admin
                        res.status(200).json({
                          success: true,
                          message: 'Bienvenido a la Api. Te has autentificado Correctamente!',
                          ip: ip,
                          usuario: usuario,
                          token: token,
                          tokenFicheros: tokenFicheros,
                          empresasPermitidas: resultadoAccesoAplicacion.empresasPermitidas
                        })
                        req.session = {
                          ip,
                          usuario,
                          empresasPermitidas: resultadoAccesoAplicacion.empresasPermitidas
                        }
                        next()
                      })
                      .catch((error) => {
                        res.status(401).json({
                          success: false,
                          message: 'Error al hacer Login, usuario/contraseña incorrectos (actu):\n ' + JSON.stringify(error)
                        })
                      })
                  } catch (error) {
                    res.status(404).json({
                      success: false,
                      message: 'Error al hacer Login, usuario/contraseña incorrectos (try2): \n' + JSON.stringify(error)
                    })
                  }
                })
                .catch((error) => {
                  res.status(401).json({
                    success: false,
                    message: 'Error al hacer Login, usuario/contraseña incorrectos (acc):\n ' + JSON.stringify(error)
                  })
                })
            }
          })
          .catch(error => {
            res.status(401).json({
              success: false,
              message: 'Error al hacer Login, usuario/contraseña incorrectos (ena):\n ' + JSON.stringify(error)
            })
          })
      } catch (error) {
        res.status(401).json({
          success: false,
          message: 'Error al hacer Login, usuario/contraseña incorrectos (try1):\n ' + JSON.stringify(error)
        })
      }
    }
  }
}

export default loginUserEnantarFactory
