# nnttr

Proyecto para construir una librería de funciones y componentes que se puedan compartir en los proyectos javascript del grupo Enantar.

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)

## Contenido de la Libreria

La librería, por defecto, carga el objeto para la parte del cliente, para usar la del servidor hay que hacer 
```javascript
 const nnttr = require('nnttr/lib/server')
```

### utils
Dentro de utils tenemos las siguientes funciones:

|Funcion|parámetros|descripción|
|---|---|---|
|capitalizeFirstLetter(string)|Se le pasa una cadena|Devuelve el mismo string pero con el primer caracter en mayúscula|
|dynamicSort(property)|se le pasa el nombre de una propiedad|Usado como función que se le tiene que pasar al método **sort** de un array de objetos, se le pasa como parámetro el nombre de la propiedad por la que queremos ordenar el array. Si se pone un `-` antes del nombre se entiende que el orden es descendente|
|dynamicSortMultiple(property1, property2, ..., propertyN)|se le pasa varios nombres de propiedad como parámetros|Permite realizar la ordenación por varias propiedades de un objeto|


#### Ejemplos:

##### Uso de dynamicSort
```javascript
 People.sort(dynamicSort("Surname")); Ascendente
 People.sort(dynamicSort("-Surname")); Descendente
```

#### Uso de dynamicSortMultiple

```javascript
 People.sort(dynamicSortMultiple("Surname", "Name")); Ascendente
 People.sort(dynamicSortMultiple("Surname","-Name")); Descendente
```


## Instalación y uso en proyectos

Para usarlo en los proyectos tenemos que lanzar el comando:

Primero pasamos este comando de git:

```bash
git config --global http.sslverify false
```

luego ya podemos hacer:

```bash
npm install git+https://git.nnttr.com/lib/nnttr.git#produccion --save
```

Esto instalará la versión en producción que tengamos actualmente en la libreria.
Cuando queramos forzar a que nuestro proyecto actualize la librería a la última versión ponemos:

```bash
npm update nnttr
```

Para hacer uso de la librería sólo tenemos que importarla:

```javascript
// Para la parte cliente
import nnttr from 'nnttr'

// para la parte servidor
const nnttr = require('nnttr/lib/server')
```


## Información para el desarrollador

Realizar siempre cambios en una rama, no en master.

### Preparación del repositorio

Añadir un pre-commit hook a git para asegurarnos que al hacer un commit el código se ajusta a javascript standard

Para ello tenemos que abrir una ventana de línea de comandos y desde la carpeta del proyecto cambiarnos a la carpeta hooks:

```bash
cd .git/hooks

# Creamos el fichero
touch pre-commit

# Le damos permisos de ejecución
chmod +x pre-commit
```

Editamos el fichero que hemos creado y añadimos lo siguiente:

```bash
#!/bin/bash 
 
AZUL='\033[1;34m'
ROJO='\033[0;31m'
VERDE='\033[0;32m'
NC='\033[0m'
AMARILLO='\033[1;33m'

# Asegura que todos los archivos javascript especificados 
function xargs-r() {
  # Portable version of "xargs -r". The -r flag is a GNU extension that 
  # prevents xargs from running if there are no input files. 
  
  if IFS= read -r -d '' path; then
    {  echo -n "$path"; echo -ne "\0"; cat;  } | xargs $@ --verbose | snazzy
  fi
}

git diff -z --name-only --cached --relative | grep -z '\.jsx\?$' | xargs-r -0 standard 

if [[ $? -ne 0 ]]; then
  echo -e "${ROJO}ABORTING COMMIT. JavaScript Standard Style errors were detected.${NC}"
  exit 1
fi
```

Para poder ejecutar comandos de node/npm desde el bash de windows, tenemos que añadir la ruta de los comandos node y npm al path

para ello abrimos al fichero .bashrc de nuestra carpeta home y añadimos al final la línea (o la carpeta donde esté instalado node):

```bash
export PATH=$PATH:"/c/Program Files/nodejs/"
```

Con esto nos aseguramos que NO se haga commit sin que previamente se haya revisado el código y se haya ajustado al estilo standard.

**PROBLEMA en WebStorm**
Si hacemos commit desde webstorm nos va a dar siempre error este hook y nunca nos va a hacer commit, ya que por alguna razón desde el git del webstorm no se tiene acceso a node y por lo tanto no es capaz de ejecutar los comandos standard y snazzy

de momento **desactivamos** este hook a la espera de ver si podemos controlar esto y solucionarlo.

### Entorno de Desarrollo

Para desarrollar tenemos que lanzar el comando:
```bash
npm run devClient
# O bien
npm run devServer
```
Esto compilará nuestra librería y la recompilará cada vez que cambiemos el código fuente de algo que esté dentro de ./src

y también hay que lanzar el siguiente comando si tenemos diseñados los tests:
```bash
npm run test:watch
```
Para que los cambios que hagamos en los *_test.spec.js se recompilen automáticamente.

### Actualizar Versión

Cuando los cambios se hayan probado y terminado hay que hacer los siguientes pasos:

* cerrar la rama con `gff`
* hacer un `run build` para que se genere la librería mimificada
* Como la librería se genera en el servidor linux, hay que hacer un download de la carpeta `lib` a nuestra máquina windows para que la librería compilada y mimificada se incorpore al control de versiones.
* Cambiar en el **package.json** el número de la versión
* Hacer commit de los cambios
* crear una etiqueta con el comando `git tag 1.0.0` con el número de versión que corresponda
* subir los cambios con un `gpo`
* publicar en pre-producción con un `pre`
* publicar en producción con un `prod`

Cuando se haya publicado una nueva versión a producción hay que hacer un `npm update nnttr` en los proyectos que utilicen la librería.

El código fuente está en la carpeta **./src**, es necesario transpilar el proyecto con el comando `npm run build` para que se genere el archivo correspondiente en **./lib** que será lo que se exporte como módulo.

Si queremos pasar los test ejecutamos: `npm run test`

Si estamos desarrollando y queremos que nos vaya haciendo la compilación según grabamos: `npm run devClient` ó `npm run devServer`

Si estamos desarrollando los tests podemos hacer que se ejecuten cada vez que grabemos los cambios con `npm run test:watch`


## Documentación y enlaces de referencia


* [Creando módulos con node.js](http://www.nodehispano.com/2012/02/creando-modulos-con-node-js-nodejs/)
* [How to test your new NPM module without publishing it every 5 minutes](https://medium.com/@the1mills/how-to-test-your-npm-module-without-publishing-it-every-5-minutes-1c4cb4b369be)
* [Cómo construir un paquete para npm](http://www.sergiolepore.net/2012/10/18/como-construir-un-modulo-de-nodejs-e-integrarlo-con-npm/)
* [Start your own JavaScript library using webpack and ES6](http://krasimirtsonev.com/blog/article/javascript-library-starter-using-webpack-es6)


* [Getting Node.js Testing and TDD Right](https://blog.risingstack.com/getting-node-js-testing-and-tdd-right-node-js-at-scale/)


* [JavaScript style guide, with linter & automatic code fixer](https://github.com/feross/standard#install)
* [Reglas válidas Javascript standard](https://standardjs.com/rules.html)


[![js-standard-style](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)

